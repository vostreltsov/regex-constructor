#ifndef TRANSLATION_H
#define TRANSLATION_H

#define APP_TITLE              "Easy regex 2.0"
#define LAB_CHARSET_ALLOWED    "Allowed characters:"
#define LAB_CHARSET_DISALLOWED "Disallowed characters:"
#define CHARSET_ANY_FROM       "Any character from"
#define CHARSET_ANY_EXCEPT     "Any character except"
#define UNTITLED_FILE_NAME     "Untitled"
#define ASSERT_EMPTY           "No assertion"
#define ASSERT_CIRCUMFLEX      "String beginning"
#define ASSERT_DOLLAR          "String ending"
#define ASSERT_BOUND           "A word boundary"
#define ASSERT_NOT_BOUND       "Not a word boundary"

#endif
