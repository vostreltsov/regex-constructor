#ifndef GRAPHDRAGCONTAINER_H
#define GRAPHDRAGCONTAINER_H

#include "regexabstractnode.h"
#include "regexgraphicscontainernode.h"
#include "globalconstants.h"


class GraphDragContainer : public RegexAbstractNode
{
    Q_OBJECT
public:
    explicit GraphDragContainer(QGraphicsScene *scene, QGraphicsItem *parent = NULL);
    ~GraphDragContainer();
    void acquire(QList<QGraphicsItem *> nodes);
    QRectF boundingRect() const;
    virtual bool changeContainer();
    bool isEmpty();
    static QList<RegexAbstractNode *> getPureNodes(QList<QGraphicsItem *> nodes);
    void release();
    QList<RegexAbstractNode *> getContained();
signals:

public slots:

protected:
    QPen borderPen;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    QList<RegexAbstractNode *> contained;
    RegexGraphicsContainerNode *previous;
    QRectF bRect;

};

#endif // GRAPHDRAGCONTAINER_H
