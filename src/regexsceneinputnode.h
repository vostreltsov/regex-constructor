#ifndef REGEXSCENEINPUTNODE_H
#define REGEXSCENEINPUTNODE_H
#include <QGraphicsObject>
#include "regexabstractnode.h"

class RegexSceneInputNode : public RegexAbstractNode
{
    Q_OBJECT
public:
    explicit RegexSceneInputNode(QGraphicsItem *parent = 0);

    QRectF boundingRect() const;

    QString generateRegex();

    enum {Type = UserType + 20};
    virtual int type() const;

signals:

public slots:

protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF bounding;

};

#endif // REGEXSCENEINPUTNODE_H
