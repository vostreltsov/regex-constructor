#include "regexgraphicsnodefactory.h"

QMainWindow* RegexGraphicsNodeFactory::mainWindow = NULL;

void RegexGraphicsNodeFactory::setMainWindow(QMainWindow * mW)
{
    mainWindow = mW;
}

QMainWindow * RegexGraphicsNodeFactory::getMainWindow()
{
    return mainWindow;
}

QList<int> RegexGraphicsNodeFactory::availableNodeTypes()
{
    QList<int> result;

    // result.append(RegexGraphicsNode::TYPE_NODE_ABSTRACT); This would be a big mistake ;)
    result.append(RegexGraphicsNode::TYPE_NODE_FINITE_QUANT);
    result.append(RegexGraphicsNode::TYPE_NODE_INFINITE_QUANT);
    result.append(RegexGraphicsNode::TYPE_NODE_SUBPATT);
    result.append(RegexGraphicsNode::TYPE_NODE_GROUP);
    result.append(RegexGraphicsNode::TYPE_LEAF_CHARSET);
    //result.append(RegexGraphicsNode::TYPE_LEAF_ASSERT); Now only on arcs
    result.append(RegexGraphicsNode::TYPE_LEAF_DOT);
    result.append(RegexGraphicsNode::TYPE_LEAF_STRING);

    return result;
}

RegexGraphicsNode * RegexGraphicsNodeFactory::get(int type, QGraphicsItem * parent, qreal x, qreal y)
{
    RegexGraphicsNode* result = NULL;

    switch (type) {
    case RegexGraphicsNode::TYPE_NODE_FINITE_QUANT:
        result = new RegexGraphicsFiniteQuant(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_NODE_INFINITE_QUANT:
        result = new RegexGraphicsInfiniteQuant(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_NODE_SUBPATT:
        result = new RegexGraphicsSubpatt(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_NODE_GROUP:
        result = new RegexGraphicsGroup(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_LEAF_CHARSET:
        result = new RegexGraphicsLeafCharset(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_LEAF_ASSERT:
        result = new RegexGraphicsLeafAssert(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_LEAF_DOT:
        result = new RegexGraphicsLeafDot(parent, x, y);
        break;
    case RegexGraphicsNode::TYPE_LEAF_STRING:
        result = new RegexGraphicsLeafString(parent, x, y);
        break;
    default:
        return NULL;
    }
    if (mainWindow) {
        QObject::connect(result, SIGNAL(openPropertyEditor(RegexGraphicsNode*)),
                         mainWindow, SLOT(showPropertyEditor(RegexGraphicsNode*)));
    }
    return result;
}
