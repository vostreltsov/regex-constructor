#ifndef GRAPHRUBBERBAND_H
#define GRAPHRUBBERBAND_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QTimer>

class GraphRubberBand : public QObject, public QGraphicsRectItem
{
    Q_OBJECT

public:
    GraphRubberBand(QPointF startPos, QGraphicsScene *scene);
    bool setEndpoint(QPointF end);
public slots:
    void setCanChange();
protected:
    bool canChange;
    QPointF start;
    QPointF lastEnd;
    QBrush brush;
    QPen pen;
};

#endif // GRAPHRUBBERBAND_H
