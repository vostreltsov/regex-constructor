#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H
#include <QColor>
#include <QFont>

/** @namespace Namespace for application-wide constants. */
namespace GlobalConst
{
    static const QColor fillColor = QColor(205, 205, 205);
    static const QColor shadowColor = QColor(105, 105, 105, 200);
    static const QColor separatorColor = QColor(58, 58, 58);
    static const QColor activeSelectionColor = QColor(255, 128, 0);
    static const QColor errorColor_1 = QColor(200, 0, 0);
    static const QColor errorColor_2 = QColor(120, 10, 10);
    static const QFont labelFont = QFont("Arial", 10, QFont::Bold);
    static const QColor multipleDragColor = QColor(255, 0, 0, 200);
}
#endif // GLOBALCONSTANTS_H
