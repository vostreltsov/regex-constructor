#include "regexgraphicsconnectionarc.h"
#include "regexnodeconnection.h"
#include <QGraphicsScene>
#include <QPainterPathStroker>
#include <QPainter>
#include <QGraphicsSceneContextMenuEvent>
#include <QKeyEvent>
#include "regexgraphicsnode.h"

RegexGraphicsConnectionArc::RegexGraphicsConnectionArc(RegexNodeConnection *connection, QGraphicsItem *parent, QGraphicsScene *scene) :
    QGraphicsPathItem(parent, scene), owner(connection)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
    setFlag(QGraphicsItem::ItemIsFocusable);
}

QVariant RegexGraphicsConnectionArc::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemSelectedHasChanged) {
        if (canReactToEvents())
            owner->redraw();
    }
    return QGraphicsPathItem::itemChange(change, value);
}

int RegexGraphicsConnectionArc::type() const
{
    return Type;
}

void RegexGraphicsConnectionArc::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (canReactToEvents()) {
        event->accept();
        QGraphicsPathItem::mousePressEvent(event);
    } else {
        event->ignore();
    }
}

void RegexGraphicsConnectionArc::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

}

void RegexGraphicsConnectionArc::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

}

void RegexGraphicsConnectionArc::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    //if (owner->getPayload() && owner->getPayload()->hasProperties())
    //    owner->getPayload()->emitPropertiesSignal(owner);
    emit owner->showArcPropertyEditor();
}

void RegexGraphicsConnectionArc::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (canReactToEvents() && !owner->isFake()) {
        QMenu menu;
        menu.addAction("Remove", owner, SLOT(removeByUser()), QKeySequence("X, Del"));
        if (owner->getPayload())
            menu.addAction("Properties", owner, SLOT(showArcPropertyEditor()));
        menu.exec(event->screenPos());
    } else {
        event->ignore();
    }
}

QPainterPath RegexGraphicsConnectionArc::shape() const
{
    QPainterPathStroker stroker;
    stroker.setWidth(pen().widthF() * 5);
    return stroker.createStroke(path());
}

void RegexGraphicsConnectionArc::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawPath(path());
}

void RegexGraphicsConnectionArc::keyPressEvent(QKeyEvent *event)
{
    if (canReactToEvents()) {
        if (event->key() == Qt::Key_X || event->key() == Qt::Key_Delete)
            owner->removeByUser();
    }
}

void RegexGraphicsConnectionArc::keyReleaseEvent(QKeyEvent *event)
{
}

bool RegexGraphicsConnectionArc::canReactToEvents() const
{
    return owner->getDestination() != NULL; // can react only if connection established
}

RegexNodeConnection *RegexGraphicsConnectionArc::getOwner()
{
    return owner;
}
