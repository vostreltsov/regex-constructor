#include "toolboxscenecontainer.h"

ToolboxSceneContainer::ToolboxSceneContainer(QObject *parent) :
    QObject(parent),
    scene(parent)
{
    // Initialize available nodes.
    QList<int> types = RegexGraphicsNodeFactory::availableNodeTypes();
    foreach (int type, types)
        availableNodes.push_back(RegexGraphicsNodeFactory::get(type));
}

ToolboxSceneContainer::~ToolboxSceneContainer()
{
    // Free memory allocated for toolbox nodes.
    foreach (RegexGraphicsNode * node, availableNodes)
        delete node;
}

QGraphicsScene* ToolboxSceneContainer::getScene()
{
    return &scene;
}

void ToolboxSceneContainer::drawToolbox()
{
    quint32 pos = 0;
    foreach (RegexGraphicsNode * node, availableNodes) {
        node->enableDrag(true);
        scene.setSceneRect(QRectF(0,
                                  0,
                                  qMax(node->boundingRect().right() + 20, scene.sceneRect().right()),
                                  scene.sceneRect().bottom() + node->boundingRect().height() + 10));

        scene.addItem(node);
        pos += node->boundingRect().height() + 10;
        node->setPos(0, pos);
    }
}
