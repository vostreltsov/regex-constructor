#include "regexgraphicsoperands.h"

// Leaf charset.
RegexGraphicsLeafCharset::RegexGraphicsLeafCharset(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsNode(parent, x, y)
{
    negative = false;
    singleChars = "abc";
    label = CHARSET_ANY_FROM;
    updateText();
}

bool RegexGraphicsLeafCharset::isNegative() const
{
    return negative;
}

QString RegexGraphicsLeafCharset::getSingleChars() const
{
    return singleChars;
}

QStringList RegexGraphicsLeafCharset::getRanges() const
{
    return ranges;
}

QStringList RegexGraphicsLeafCharset::getPosixClasses() const
{
    return posixClasses;
}

void RegexGraphicsLeafCharset::setNegative(bool value)
{
    negative = value;
    updateText();
}

void RegexGraphicsLeafCharset::setSingleChars(const QString & value)
{
    singleChars = value;
    updateText();
}

void RegexGraphicsLeafCharset::setRanges(const QStringList & value)
{
    ranges = value;
    updateText();
}

void RegexGraphicsLeafCharset::setPosixClasses(const QStringList & value)
{
    posixClasses = value;
    updateText();
}

void RegexGraphicsLeafCharset::setEverything(bool negative, const QString & singleChars, const QStringList&  ranges, const QStringList & posix)
{
    this->negative = negative;
    this->singleChars = singleChars;
    this->ranges = ranges;
    this->posixClasses = posix;
    updateText();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsLeafCharset::nodeType() const
{
    return RegexGraphicsNode::TYPE_LEAF_CHARSET;
}

QString RegexGraphicsLeafCharset::generateRegex(bool *previousNodeGrouped)
{
    QString result;
    QString special("^-[]\\");

    // Adding single characters.
    for (QString::const_iterator iter = singleChars.constBegin(); iter != singleChars.constEnd(); ++iter) {
        QString toAdd("");
        if (special.contains(*iter))
            toAdd = QString("\\") + *iter;
        else
            toAdd = *iter;

        if (!result.contains(toAdd))
            result += toAdd;
    }
    // Adding ranges.
    for (QStringList::const_iterator iter = ranges.constBegin(); iter != ranges.constEnd(); ++iter) {
        QString tmp = *iter;
        int delimPos = tmp.indexOf(QRegExp(".-"), 0);
        QString start = tmp.left(delimPos + 1);
        QString end = tmp.mid(delimPos + 2);
        if (start.startsWith('-') || start.startsWith('\\')) {
            start = '\\' + start;
        }
        if (end.startsWith('-') || end.startsWith('\\')) {
            end = '\\' + end;
        }
        result += start + "-" + end;
    }

    // Adding negativeness and brackets.
    if (negative)
        result = "^" + result;
    result = "[" + result + "]";
    *previousNodeGrouped = true;
    return result;
}

void RegexGraphicsLeafCharset::updateText()
{
    label = negative ? CHARSET_ANY_EXCEPT : CHARSET_ANY_FROM;

    // Adding single characters.
    QString part1 = singleChars;

    // Adding ranges.
    QString part2;
    for (QStringList::const_iterator iter = ranges.constBegin(); iter != ranges.constEnd(); ++iter) {
        QString tmp = *iter;
        int delimPos = tmp.indexOf(QRegExp(".-"), 0);
        QString start = tmp.left(delimPos + 1);
        QString end = tmp.mid(delimPos + 2);
        part2 += "from '" + start + "' to '" + end + "'\n";
    }
    part2 = part2.left(part2.length() - 1);

    // Adding POSIX classes.
    /*
    QString part3;
    QMap<QString, QString> map = getPosixMap();
    for (QStringList::const_iterator iter = posixClasses.constBegin(); iter != posixClasses.constEnd(); ++iter) {
        QString desc = *iter;
        bool negative = desc.startsWith("not ");
        if (negative) {
            desc = desc.mid(4);
        }
        QString tmp = map.find(desc).value();

        if (negative) {
            part3 += "[:^" + tmp + ":]";
        } else {
            part3 += "[:" + tmp + ":]";
        }
    }*/

    QString newText = part1;
    if (newText.length() > 0 && part2.length() > 0) {
        newText += "\n" + part2;
    } else if (newText.length() == 0 && part2.length() > 0) {
        newText = part2;
    }

    // Adding negativeness and brackets.
    text = newText;
    RegexGraphicsNode::updateText();
}

QMap<QString, QString> RegexGraphicsLeafCharset::getPosixMap() {
    QMap<QString, QString> POSIXClasses;
    POSIXClasses.insert("alphanumeric", "alnum");
    POSIXClasses.insert("alphabetic", "alpha");
    POSIXClasses.insert("ascii", "ascii");
    POSIXClasses.insert("space or tab", "blank");
    POSIXClasses.insert("control character", "cntrl");
    POSIXClasses.insert("decimal digit", "digit");
    POSIXClasses.insert("printing, excluding space", "graph");
    POSIXClasses.insert("lower case letter", "lower");
    POSIXClasses.insert("printing, including space", "print");
    POSIXClasses.insert("printing, excluding alphanumeric", "punct");
    POSIXClasses.insert("white space", "space");
    POSIXClasses.insert("upper case letter", "upper");
    POSIXClasses.insert("word character", "word");
    POSIXClasses.insert("hexadecimal digit", "xdigit");
    return POSIXClasses;
}

QList<RegexAbstractNode::PropertyPair> RegexGraphicsLeafCharset::getProperties()
{
    QList<PropertyPair> list;
    list<<qMakePair(QString("negative"), QString::number(isNegative()));
    list<<qMakePair(QString("chars"), QString(getSingleChars()));
    list<<qMakePair(QString("ranges"), getRanges().join("=="));
    return list;
}

void RegexGraphicsLeafCharset::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{
    foreach (PropertyPair pair, props) {
        if (pair.first == "negative") {
            setNegative(pair.second.toInt());
        } else if (pair.first == "chars") {
            setSingleChars(pair.second);
        } else if (pair.first == "ranges") {
            setRanges(pair.second.split("==", QString::SkipEmptyParts));
        }
    }
}

// Leaf assert.
RegexGraphicsLeafAssert::RegexGraphicsLeafAssert(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsNode(parent, x, y)
{
    label = QString("Simple assertion");
    setAssertion(ASSERT_BOUND);
}

void RegexGraphicsLeafAssert::setAssertion(QString value)
{
    if (assertion != value) {
        assertion = value;
        updateText();
    }
}

QString RegexGraphicsLeafAssert::getAssertion() const
{
    return assertion;
}

RegexGraphicsNode::RegexNodeType RegexGraphicsLeafAssert::nodeType() const
{
    return RegexGraphicsNode::TYPE_LEAF_ASSERT;
}

QString RegexGraphicsLeafAssert::generateRegex(bool *previousNodeGrouped)
{
    *previousNodeGrouped = true;
    if (assertion == ASSERT_CIRCUMFLEX) {
        return "^";
    } else if (assertion == ASSERT_DOLLAR) {
        return "$";
    } else if (assertion == ASSERT_BOUND) {
        return "\\b";
    } else if (assertion == ASSERT_NOT_BOUND) {
        return "\\B";
    } else {
        return "";
    }
}

void RegexGraphicsLeafAssert::updateText()
{
    text = assertion;
    RegexGraphicsNode::updateText();
}

QList<RegexAbstractNode::PropertyPair> RegexGraphicsLeafAssert::getProperties()
{
    QList<PropertyPair> list;
    list<<qMakePair(QString("assert"), QString(getAssertion()));
    return list;
}

void RegexGraphicsLeafAssert::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{
    foreach (PropertyPair pair, props) {
        if (pair.first == "assert") {
            setAssertion(pair.second);
        }
    }
}

// Leaf dot.
RegexGraphicsLeafDot::RegexGraphicsLeafDot(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsNode(parent, x, y)
{
    label = QString("Any character");
    text = QString("Any");
    RegexGraphicsNode::updateText();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsLeafDot::nodeType() const
{
    return RegexGraphicsNode::TYPE_LEAF_DOT;
}

QString RegexGraphicsLeafDot::generateRegex(bool *previousNodeGrouped)
{
    *previousNodeGrouped = true;
    return ".";
}

bool RegexGraphicsLeafDot::hasProperties() const
{
    return false;
}

// Leaf string.
RegexGraphicsLeafString::RegexGraphicsLeafString(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsNode(parent, x, y)
{
    label = QString("String \"as is\"");
    text = QString("write a string here");
    updateText();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsLeafString::nodeType() const
{
    return RegexGraphicsNode::TYPE_LEAF_STRING;
}

QString RegexGraphicsLeafString::generateRegex(bool *previousNodeGrouped)
{
    QString result = QRegExp::escape(text);
    *previousNodeGrouped &= (result.length() <= 1);
    //if (result.length() > 1) {
    //    result = "(?:" + result + ")";
    //}
    return result;
}
void RegexGraphicsLeafString::setText(QString value)
{
    if (text != value) {
        text = value;
        RegexGraphicsNode::updateText();
    }
}

QList<RegexAbstractNode::PropertyPair> RegexGraphicsLeafString::getProperties()
{
    QList<PropertyPair> list;
    list<<qMakePair(QString("text"), getText());
    return list;
}

void RegexGraphicsLeafString::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{
    foreach (PropertyPair pair, props) {
        if (pair.first == "text") {
            setText(pair.second);
        }
    }
}
