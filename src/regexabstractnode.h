#ifndef REGEXABSTRACTNODE_H
#define REGEXABSTRACTNODE_H

#include <QGraphicsObject>
#include <QList>

class RegexNodeInput;
class RegexNodeOutput;
class RegexNodeIOInterface;
class RegexGraphicsContainerNode;


class RegexAbstractNode : public QGraphicsObject
{
    Q_OBJECT
public:

    explicit RegexAbstractNode(QGraphicsItem *parent = 0);
    virtual ~RegexAbstractNode();
    bool isInputActive() const;
    bool isOutputActive() const;

    RegexNodeInput* getInput() const;
    RegexNodeOutput* getOutput() const;
    virtual QString generateRegex(bool *previousNodeGrouped);

    /** @brief Returns true if 'this' is an item ancestor */
    bool parentOf(QGraphicsItem *item);

    typedef QPair<QString, QString> PropertyPair;

    /**
     * @brief Returns true if node has any editable properties.
     */
    virtual bool hasProperties() const;

    virtual QList<PropertyPair> getProperties();
    virtual void setProperties(QList<PropertyPair> props);

    /**
      * @brief If true item can be dragged from toolbox.
      */
    bool isInToolbox() const;
    virtual int type() const;
    enum {Type = UserType + 101 };

    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    /** @brief Workaroud for a bug when Qt ignoring IntersectsItemBoundingRect in arguments. */
    virtual bool collidesWithItem(const QGraphicsItem * other, Qt::ItemSelectionMode mode = Qt::IntersectsItemShape) const;

    /** @brief Tries to change container for this node to an item underneath.
      * @return true if container changed.
    */
    virtual bool changeContainer();

    static RegexAbstractNode *findBottomNode(QList<RegexAbstractNode *> list);

signals:

public slots:
    void activateInput();
    void activateOutput();
    void deactivateInput();
    void deactivateOutput();
    virtual void shiftCollidingItems(QGraphicsItem *ignore = NULL);

    /** @brief Slot that is called in case of arc connection/disconnection, on default does nothing. */
    virtual void ioStateChanged();

    /** @brief Removes this node from graph. */
    void removeNode();

    /** @brief Removes node and remembers graph state for undo. */
    void removeByUser();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    RegexGraphicsContainerNode * findPossibleContainer();

    /** @var Input node */
    RegexNodeInput *input;

    /** @var Output node, will probably change in the future for some sort of container. */
    RegexNodeOutput *output;

    /** @var Is drag-n-drop from toolbox possible? */
    bool inToolbox;

    RegexGraphicsContainerNode *containerHover;

};

#endif // REGEXABSTRACTNODE_H
