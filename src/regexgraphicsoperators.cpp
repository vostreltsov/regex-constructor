#include "regexgraphicsoperators.h"

void RegexGraphicsOperator::updateLabel()
{
    label = QString("Operator");
}

RegexGraphicsOperator::RegexGraphicsOperator(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsContainerNode(parent, x, y)
{
    updateLabel();
}

// Operator finite quant.
void RegexGraphicsFiniteQuant::updateLabel()
{
    setLabel(QString("From ") + leftBorder + QString(" to ") + rightBorder + QString(" times"));
}

RegexGraphicsFiniteQuant::RegexGraphicsFiniteQuant(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsOperator(parent, x, y)
{
    leftBorder = QString("x");
    rightBorder = QString("y");
    updateLabel();
}

QString RegexGraphicsFiniteQuant::getLeftBorder() const
{
    return leftBorder;
}

QString RegexGraphicsFiniteQuant::getRightBorder() const
{
    return rightBorder;
}

void RegexGraphicsFiniteQuant::setLeftBorder(const QString & value)
{
    leftBorder = value;
    updateLabel();
}

void RegexGraphicsFiniteQuant::setRigntBorder(const QString & value)
{
    rightBorder = value;
    updateLabel();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsFiniteQuant::nodeType() const
{
    return RegexGraphicsNode::TYPE_NODE_FINITE_QUANT;
}

QString RegexGraphicsFiniteQuant::generateRegex(bool *previousNodeGrouped)
{
    QString body = RegexGraphicsOperator::generateRegex(previousNodeGrouped);
    if (body.length() > 1 && !(*previousNodeGrouped)) {
        body = "(?:" + body + ")";
    }
    QString left = QString::number(leftBorder.toInt());
    QString right = QString::number(rightBorder.toInt());
    *previousNodeGrouped = true;
    if (left == "0" && right == "1") {
        return body + "?";
    } else {
        return body + "{" + left + "," + right + "}";
    }
}

QList<RegexAbstractNode::PropertyPair> RegexGraphicsFiniteQuant::getProperties()
{
    QList<PropertyPair> list;
    list<<qMakePair(QString("leftborder"), leftBorder);
    list<<qMakePair(QString("rightborder"), rightBorder);
    return list;
}

void RegexGraphicsFiniteQuant::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{
    foreach (PropertyPair pair, props) {
        if (pair.first == "leftborder") {
            setLeftBorder(pair.second);
        } else if (pair.first == "rightborder") {
            setRigntBorder(pair.second);
        }
    }
}


// Operator infinite quant.
void RegexGraphicsInfiniteQuant::updateLabel()
{
    setLabel(QString("From ") + leftBorder + QString(" to infinity times"));
}

RegexGraphicsInfiniteQuant::RegexGraphicsInfiniteQuant(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsOperator(parent, x, y)
{
    leftBorder = QString("x");
    updateLabel();
}

QString RegexGraphicsInfiniteQuant::getLeftBorder() const
{
    return leftBorder;
}

void RegexGraphicsInfiniteQuant::setLeftBorder(const QString & value)
{
    leftBorder = value;
    updateLabel();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsInfiniteQuant::nodeType() const
{
    return RegexGraphicsNode::TYPE_NODE_INFINITE_QUANT;
}

QString RegexGraphicsInfiniteQuant::generateRegex(bool *previousNodeGrouped)
{
    QString body = RegexGraphicsOperator::generateRegex(previousNodeGrouped);
    if (body.length() > 1 && !(*previousNodeGrouped)) {
        body = "(?:" + body + ")";
    }
    QString left = QString::number(leftBorder.toInt());
    *previousNodeGrouped = true;
    if (left == "0") {
        return body + "*";
    } else if (left == "1") {
        return body + "+";
    } else {
        return body + "{" + left + ",}";
    }
}

QList<RegexAbstractNode::PropertyPair> RegexGraphicsInfiniteQuant::getProperties()
{
    QList<PropertyPair> list;
    list<<qMakePair(QString("leftborder"), leftBorder);
    return list;
}

void RegexGraphicsInfiniteQuant::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{
    foreach (PropertyPair pair, props) {
        if (pair.first == "leftborder") {
            setLeftBorder(pair.second);
        }
    }
}

// Operator subpatt.
void RegexGraphicsSubpatt::updateLabel()
{
    // TODO: subpattern number?
    label = QString("Subpattern");
}

RegexGraphicsSubpatt::RegexGraphicsSubpatt(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsOperator(parent, x, y)
{
    updateLabel();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsSubpatt::nodeType() const
{
    return RegexGraphicsNode::TYPE_NODE_SUBPATT;
}

QString RegexGraphicsSubpatt::generateRegex(bool *previousNodeGrouped)
{
    *previousNodeGrouped = true;
    return "(" + RegexGraphicsOperator::generateRegex(previousNodeGrouped) + ")";
}

bool RegexGraphicsSubpatt::hasProperties() const
{
    return false;
}

// Operator group.
void RegexGraphicsGroup::updateLabel()
{
    label = QString("Grouping");
}

RegexGraphicsGroup::RegexGraphicsGroup(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsOperator(parent, x, y)
{
    updateLabel();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsGroup::nodeType() const
{
    return RegexGraphicsNode::TYPE_NODE_GROUP;
}

QString RegexGraphicsGroup::generateRegex(bool *previousNodeGrouped)
{
    if (*previousNodeGrouped) {
        return RegexGraphicsOperator::generateRegex(previousNodeGrouped);
    } else {
        *previousNodeGrouped = true;
        return "(?:" + RegexGraphicsOperator::generateRegex(previousNodeGrouped) + ")";
    }
}

bool RegexGraphicsGroup::hasProperties() const
{
    return false;
}
