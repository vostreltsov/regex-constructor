#ifndef REGEXGRAPHICSNODEOUTPUT_H
#define REGEXGRAPHICSNODEOUTPUT_H

#include <QObject>
#include <QGraphicsEllipseItem>
#include "regexnodeconnection.h"
#include "regexnodeiointerface.h"
#include <QTextStream>
#include <QList>
#include <QGraphicsSceneEvent>


/** @brief Filters context menu and key events to prevent undesired behaviour during arc connection */
class ArcConnectionEventFilter : public QObject
{
    Q_OBJECT
public:
    bool eventFilter(QObject * obj, QEvent * event)
    {
        if (event->type() == QGraphicsSceneEvent::GraphicsSceneContextMenu
                || event->type() == QGraphicsSceneEvent::KeyPress) {
            return true;
        } else {
            return QObject::eventFilter(obj, event);
        }
    }
};

/** @brief Output connection point. */
class RegexNodeOutput : public RegexNodeIOInterface
{
    Q_OBJECT

protected:

    /** @var Pointer to a connection, that doesn't yet have destination node. */
    RegexNodeConnection* newConnection;

    RegexNodeInput *lastAnimatedInput;

public:
    explicit RegexNodeOutput(RegexAbstractNode *parent);
    virtual QList<RegexAbstractNode* > getAllIncidentNodes();

    enum {Type = UserType + 3};
    virtual int type() const;

signals:

public slots:
    virtual void adjustPos();

    RegexNodeConnection *addFakeOutputConnection();
    void removeFakeConnections();

protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual bool isAcceptedItem(QGraphicsItem *item) const;

    ArcConnectionEventFilter filter;
};

#endif // REGEXGRAPHICSNODEOUTPUT_H
