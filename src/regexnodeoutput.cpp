#include "regexnodeoutput.h"
#include <QCursor>
#include <QApplication>
#include <QDrag>
#include <QMimeData>
#include <QString>
#include "regexgraphicsnode.h"
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include "regexnodeinnerinput.h"
#include "regexgraphicscontainernode.h"
#include "graphscene.h"
#include "algorithms.h"
#include <QPropertyAnimation>


RegexNodeOutput::RegexNodeOutput(RegexAbstractNode *parent) :
    RegexNodeIOInterface(parent),
    newConnection(NULL),
    lastAnimatedInput(NULL)
{
    setAcceptHoverEvents(true);
    adjustPos();
    setZValue(10);
}

RegexNodeConnection* RegexNodeOutput::addFakeOutputConnection()
{
    RegexNodeConnection *conn = NULL;
    if (type() == Type && parentNode->type() != RegexSceneInputNode::Type) {
        if (connections.size() == 0) {
            QGraphicsItem* parentContainer = getParentNode()->parentItem();
            if (parentContainer && parentContainer->type() == RegexGraphicsContainerNode::Type) {
                RegexGraphicsContainerNode* container = qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentContainer);
                conn = new RegexNodeConnection(this, container->getFakeInnerInput());
                container->getFakeInnerInput()->addConnection(conn);
            } else {
                GraphScene *graphScene = qobject_cast<GraphScene *>(scene());
                if (graphScene) {
                    conn = new RegexNodeConnection(this, graphScene->getSceneOutput()->getInput());
                    graphScene->getSceneOutput()->getInput()->addConnection(conn);
                }
            }
            if (conn) {
                conn->setFake(true);
                addConnection(conn);
                conn->redraw();
            }
        }
    }
    return conn;
}

void RegexNodeOutput::removeFakeConnections()
{
    foreach (RegexNodeConnection* connection, connections) {
        if (connection->isFake())
            delete connection;
    }
}

void RegexNodeOutput::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (!parentNode->isInToolbox() && event->button() == Qt::LeftButton) {
        scene()->installEventFilter(&filter);
        event->accept();
    } else {
        event->ignore();
    }
}

void RegexNodeOutput::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton)).length()
            < QApplication::startDragDistance()
            || parentNode->isInToolbox()) {
        return;
    }
    if (newConnection == NULL) {
        removeFakeConnections();
        newConnection = new RegexNodeConnection(this);
    } else {
        QPointF scenePos = event->scenePos();
        QGraphicsItem *item = scene()->itemAt(scenePos);
        if (item && isAcceptedItem(item)) {
            QGraphicsObject *hoverOver = item->toGraphicsObject();
            RegexNodeInput *input = qobject_cast<RegexNodeInput *> (hoverOver);
            input->animateAcceptDropEnter();
            lastAnimatedInput = input;
        } else if (lastAnimatedInput) {
            lastAnimatedInput->animateAcceptDropLeave();
            lastAnimatedInput = NULL;
        }
        newConnection->setEndpoint(event->scenePos());
        newConnection->redraw();
    }
}

void RegexNodeOutput::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) { // to prevent funky mouse combinations
        if (lastAnimatedInput) {
            lastAnimatedInput->animateAcceptDropLeave();
            lastAnimatedInput = NULL;
        }
        QPointF scenePos = event->scenePos();
        QGraphicsItem* droppedOn = scene()->itemAt(scenePos);
        if (droppedOn && isAcceptedItem(droppedOn)) {

            RegexNodeIOInterface* input;
            if (droppedOn->type() == RegexNodeInput::Type) {
                input = qgraphicsitem_cast<RegexNodeInput* >(droppedOn);
            } else if (droppedOn->type() == RegexNodeInnerInput::Type) {
                input = qgraphicsitem_cast<RegexNodeInnerInput* >(droppedOn);
            } else {
                qCritical("WHAT HAVE YOU DONE?!!!\nSeriously, you accepted drop on item that is not even an input.");
            }
            static_cast<GraphScene *>(scene())->createUndoAction();
            if (input->getParentNode() == parentNode) {
                delete newConnection;
                newConnection = NULL;
            } else if (input->addConnection(newConnection)) {
                addConnection(newConnection);
                Algorithms::CycleFinderFunctor cycleFinder(input->getParentNode());
                if (Algorithms::breadthFirstSearch(parentNode, cycleFinder)) {
                    delete newConnection;
                    newConnection = NULL;
                } else {
                    parentNode->ioStateChanged();
                }
            } else {
                delete newConnection;
            }
            newConnection = NULL;
        } else if (newConnection != NULL) {
            delete newConnection;
            newConnection = NULL;
        }
        scene()->removeEventFilter(&filter);
    } else {
        event->ignore();
    }
}

void RegexNodeOutput::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    RegexNodeIOInterface::adjustPos();
    QBrush brush;
    brush.setColor(QColor(0, 0, 0));
    brush.setStyle(Qt::SolidPattern);
    painter->setBrush(brush);
    painter->drawEllipse(boundingRect());
    brush.setColor(QColor(25, 200, 25));
    painter->setBrush(brush);
    painter->drawEllipse(boundingRect().adjusted(1, 1, -1, -1));
}

void RegexNodeOutput::adjustPos()
{
    qreal xPos = parentNode->boundingRect().width() + boundingRect().width() / 2;
    qreal yPos = parentNode->boundingRect().height() / 2 + boundingRect().height() / 2;
    setPos(xPos, yPos);
    //RegexGraphicsNodeIOInterface::adjustPos();
}


int RegexNodeOutput::type() const
{
    return Type;
}

QList<RegexAbstractNode* > RegexNodeOutput::getAllIncidentNodes()
{
    QList<RegexAbstractNode* > inc;
    foreach (RegexNodeConnection *node, connections)
        inc.append(node->getDestination());
    return inc;
}


bool RegexNodeOutput::isAcceptedItem(QGraphicsItem* item) const
{
    if (item->parentItem() == parentNode)
        return false;
    int itemType = item->type();
    switch (itemType) {
    case RegexNodeInput::Type:
        if (item->parentItem()->parentItem() == parentNode->parentItem())
            return true;
        else
            return false;
        break;
    case RegexNodeInnerInput::Type:
        if (item->parentItem() == parentNode->parentItem())
            return true;
        else
            return false;
        break;
    default:
        return false;
    }
}

void RegexNodeOutput::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if (parentNode && !parentNode->isInToolbox()) {
        setTransformOriginPoint(boundingRect().center());
        QPropertyAnimation *anim = new QPropertyAnimation(this);
        anim->setDuration(100);
        anim->setTargetObject(this);
        anim->setPropertyName("scale");
        anim->setStartValue(1.0);
        anim->setEndValue(2);
        connect(anim, SIGNAL(finished()), anim, SLOT(deleteLater()));
        anim->start();
    }

}

void RegexNodeOutput::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    if (parentNode && !parentNode->isInToolbox()) {
        setTransformOriginPoint(boundingRect().center());
        QPropertyAnimation *anim = new QPropertyAnimation(this);
        anim->setDuration(100);
        anim->setTargetObject(this);
        anim->setPropertyName("scale");
        anim->setStartValue(2);
        anim->setEndValue(1.0);
        connect(anim, SIGNAL(finished()), anim, SLOT(deleteLater()));
        anim->start();
    }

}
