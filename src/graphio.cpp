#include "graphio.h"


int GraphIO::loadFromFile(QString filePath, GraphScene *loadedGraph) {

    QFile file(filePath);
    QString graphXML;

    if (!file.exists()){
        QTextStream(stdout) << "The file does not exist\n";
        return -1;
    }

    if (!file.open(QIODevice::ReadOnly)) {
        QTextStream(stdout) <<  file.errorString() << "\n";
        return -2;
    }

    QTextStream textStream(&file);
    graphXML = textStream.readAll();

    loadedGraph->clearUndoStack();
    XMLGraphSerialization::deserializeGraph(graphXML, loadedGraph);

    file.close();

    return 0;
}

int GraphIO::saveToFile(QString filePath, GraphScene *storedGraph){

    QFile file(filePath);
    QString qsGraph;

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream(stdout) <<  file.errorString() << "\n";
        return -2;
    }

    //storedGraph->removeAutoConnections();
    qsGraph = XMLGraphSerialization::serializeGraph(storedGraph);

    storedGraph->makeAutoConnections();

    QTextStream textStream(&file);

    textStream << qsGraph << endl;

    file.close();

    return 0;
}
