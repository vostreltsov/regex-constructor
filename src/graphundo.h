#ifndef GRAPHUNDO_H
#define GRAPHUNDO_H

#include <QUndoCommand>
#include "graphscene.h"

class GraphUndo : public QUndoCommand
{
public:
    GraphUndo(GraphScene *scene);
    virtual void undo();
    virtual void redo();

private:
    void serializeIn(QByteArray &arr);
    void deserializeFrom(QByteArray &arr);
    bool firstRedoCall;
    QByteArray newState;
    QByteArray oldState;
    GraphScene *graph;
};

#endif // GRAPHUNDO_H
