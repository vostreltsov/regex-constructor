#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QString>
#include <QRegExpValidator>
#include <QFileDialog>
#include <QTimer>
#include <QCloseEvent>

#include "ui_mainwindow.h"
#include "ui_testingwindow.h"
#include "testingwindow.h"
#include "graphscene.h"
#include "toolboxscenecontainer.h"
#include "regexgraphicsoperands.h"
#include "regexgraphicsoperators.h"
#include "regexnodeconnection.h"
#include "graphio.h"
#include "translation.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void lineEdit_editingFinished();
    void checkBox_stateChanged(int state);
    void comboBox_currentIndexChanged(int index);
    void listWidgetRanges_itemSetChanged();

public slots:
    void showPropertyEditor(RegexGraphicsNode * node);
    void showPropertyEditor(RegexNodeConnection * connection);
    void hidePropertyEditor();
    void refreshRegex(QString regex, bool ok);
    void sceneChanged();

    void actionNew_triggered();
    void actionOpen_triggered();
    void actionSave_triggered();
    void actionSaveAs_triggered();
    void actionExit_triggered();
    void actionUndo_triggered();
    void actionRedo_triggered();
    void actionCut_triggered();
    void actionCopy_triggered();
    void actionPaste_triggered();
    void actionSelectAll_triggered();
    void actionHelp_triggered();
    void actionAbout_triggered();
    void actionExport_triggered();

    void btnCharsetAddRange_clicked();
    void btnCharsetAddPosix_clicked();
    void btnCharsetRemoveSelected_clicked();

    void btnHideProperties_clicked();

    void btnTestRegex_clicked();

private:
    Ui::MainWindow *ui;
    TestingWindow * testingWindow;
    GraphScene *scene;
    ToolboxSceneContainer *toolbox;
    QRegExpValidator *validatorInt;
    QRegExpValidator *validatorChar;
    RegexGraphicsNode *editingNode;
    RegexNodeConnection * editingConnection;

    QMessageBox::StandardButton youSure();

    void savePropsCharset();
    void savePropsAssertion();
    void savePropsFiniteQuant();
    void savePropsInfiniteQuant();
    void savePropsString();

    void loadPropsCharset(RegexGraphicsLeafCharset * node);
    void loadPropsAssert(RegexGraphicsLeafAssert * node);
    void loadPropsFiniteQuant(RegexGraphicsFiniteQuant * node);
    void loadPropsInfiniteQuant(RegexGraphicsInfiniteQuant * node);
    void loadPropsString(RegexGraphicsLeafString * node);
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
