#include "regexabstractnode.h"
#include "regexnodeconnection.h"
#include "globalconstants.h"
#include "regexnodeinput.h"
#include "regexnodeoutput.h"
#include "graphscene.h"
#include <QVector2D>

RegexNodeConnection* RegexNodeConnection::createConnection(RegexAbstractNode *from, RegexAbstractNode *to)
{
    RegexNodeConnection* connection = NULL;
    if (from->isOutputActive() && to->isInputActive()) {
        connection = new RegexNodeConnection(from->getOutput(), to->getInput());
        from->getOutput()->addConnection(connection);
        to->getInput()->addConnection(connection);
        from->ioStateChanged();
    }
    return connection;
}




RegexNodeConnection::RegexNodeConnection(RegexNodeOutput *_from, RegexNodeInput *_to, QObject *parent) :
    QObject(parent),
    sourceOut(_from),
    destIn(_to),
    from(_from->getParentNode()),
    endpoint(_from->scenePos()),
    validity(true),
    fake(false),
    payload(NULL),
    payloadText(NULL)
{
    if(_to)
        to =_to->getParentNode();
    else
        to = NULL;


    pathItem = new RegexGraphicsConnectionArc(this, NULL, from->scene());
    arcScene = qobject_cast<GraphScene *>(from->scene());

    connect(this, SIGNAL(showPropertyEditor(RegexNodeConnection *)),
            RegexGraphicsNodeFactory::getMainWindow(),
            SLOT(showPropertyEditor(RegexNodeConnection*)));
    redraw();
    pathItem->setZValue(-100); // magic
}

RegexNodeConnection::~RegexNodeConnection()
{
    if (pathItem)
        delete pathItem;
    if (sourceOut) {
        sourceOut->removeConnection(this);
        if (!isFake())
            from->ioStateChanged();
    }
    if (destIn) {
        destIn->removeConnection(this);
        if (!isFake())
            to->ioStateChanged();
    }

}


void RegexNodeConnection::setPayload(RegexGraphicsNode *node)
{
    if (node && payload != node) {
        if (payload) {
            payload->removeNode();
            delete payloadText;
            payload = NULL;
            payloadText = NULL;
        }
        payload = node;
        payloadText = new QGraphicsTextItem(pathItem, from->scene());
    } else if (node == NULL && payload) {
        payload->removeNode();
        delete payloadText;
        payloadText = NULL;
        payload = NULL;
    }
}

RegexGraphicsNode *RegexNodeConnection::getPayload()
{
    return payload;
}

QString RegexNodeConnection::generatePayloadRegex(bool *previousNodeGrouped)
{
    Q_ASSERT(payload != NULL);
    return payload->generateRegex(previousNodeGrouped);
}

QPainterPath RegexNodeConnection::constructArc()
{
    static const QPolygonF arrowPoly = QPolygonF()
           <<QPointF(0, 0)
           <<QPointF(-10, -3)
           <<QPointF(-8, 0)
           <<QPointF(-10, 3)
           <<QPointF(0, 0);
    QPointF fromP, toP, midPUp, midPDn;
    fromP = sourceOut->sceneBoundingRect().center();
    if (destIn != NULL && destIn->getParentNode() != NULL)
        toP = destIn->sceneBoundingRect().center();
    else
        toP = endpoint;
    qreal curvature = ((toP - fromP).manhattanLength()) / 5;

    qreal maxCurvature = 30; // pseudo-random value
    curvature = qMin(qMax(curvature, 0.0), maxCurvature);
    midPDn = fromP + (toP - fromP) / 2 + QPoint(-10,  curvature);
    midPUp = fromP + (toP - fromP) / 2 - QPoint(-10,  curvature);
    QPainterPath path(fromP);
    path.cubicTo(midPUp, midPDn, toP);

    QTransform arrowTransform;
    QPointF arrowPoint;
    arrowPoint = path.pointAtPercent(path.percentAtLength(path.length() - 4));
    arrowTransform.translate(arrowPoint.x(), arrowPoint.y());
    arrowTransform.rotate(-path.angleAtPercent(1));
    QPolygonF rotatedArrow = arrowTransform.map(arrowPoly);
    path.addPolygon(rotatedArrow);
    return path;
}


void RegexNodeConnection::redraw()
{
    if (!isValid() && pathItem->isSelected()) {
        QPen pen(GlobalConst::errorColor_2, 6);
        pen.setStyle(Qt::DashDotLine);
        pathItem->setPen(pen);
    } else if (!isValid()) {
        QPen pen(GlobalConst::errorColor_1, 6);
        pen.setStyle(Qt::DashDotLine);
        pathItem->setPen(pen);
    } else if (isFake()) {
        QPen pen(Qt::gray, 3);
        pen.setStyle(Qt::DashLine);
        pathItem->setPen(pen);
    } else if (pathItem->isSelected()) {
        pathItem->setPen(QPen(GlobalConst::activeSelectionColor, 3));
    } else {
        pathItem->setPen(QPen(Qt::black, 3));
    }

    pathItem->setPath(constructArc());
    if (payload) {
        QTransform rotation;
        rotation.rotate(-pathItem->path().angleAtPercent(0.5));
        payloadText->setPlainText(payload->getText());
        QPointF textPos = pathItem->path().pointAtPercent(0.5);
        QPointF adjustment(-payloadText->boundingRect().width() / 2, 15);
        adjustment = rotation.map(adjustment);
        payloadText->setPos(textPos + adjustment);
        payloadText->setRotation(-pathItem->path().angleAtPercent(0.5));
    }
}

void RegexNodeConnection::setDestination(RegexNodeInput* dest)
{
    if(destIn == NULL) {
        destIn = dest;
        to = destIn->getParentNode();
    }
}

void RegexNodeConnection::setEndpoint(const QPointF& point)
{
    endpoint = point;
}

RegexAbstractNode* RegexNodeConnection::getSource() const
{
    return from;
}

RegexAbstractNode* RegexNodeConnection::getDestination() const
{
    return to;
}


RegexNodeOutput* RegexNodeConnection::getSourceOutput() const
{
    return sourceOut;
}

RegexNodeInput* RegexNodeConnection::getDestinationInput() const
{
    return destIn;
}

void RegexNodeConnection::setValid(bool valid)
{
    if (valid != validity) {
        validity = valid;
        redraw();
    }
}

bool RegexNodeConnection::isValid()
{
    return validity;
}

void RegexNodeConnection::setFake(bool fake)
{
    this->fake = fake;
    pathItem->setFlag(QGraphicsItem::ItemIsSelectable, !fake);
    pathItem->setFlag(QGraphicsItem::ItemIsFocusable, !fake);
}

bool RegexNodeConnection::isFake()
{
    return fake;
}

void RegexNodeConnection::removeByUser()
{
    GraphScene *sc = static_cast<GraphScene *>(pathItem->scene());
    sc->createUndoAction();
    deleteLater();
}

void RegexNodeConnection::showPayloadPropertyEditor()
{
    getPayload()->emitPropertiesSignal(this);
}

void RegexNodeConnection::showArcPropertyEditor()
{
    emit showPropertyEditor(this);
}
