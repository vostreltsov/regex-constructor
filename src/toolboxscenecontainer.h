#ifndef TOOLBOXSCENECONTAINER_H
#define TOOLBOXSCENECONTAINER_H

#include <QObject>
#include <QGraphicsScene>
#include <QPair>
#include <QList>
#include "regexgraphicsnode.h"
#include "regexgraphicsnodefactory.h"

class ToolboxSceneContainer : public QObject
{
    Q_OBJECT
public:
    explicit ToolboxSceneContainer(QObject *parent = 0);
    ~ToolboxSceneContainer();

    /**
      * @brief Returns a pointer to the toolbox scene.
      */
    QGraphicsScene* getScene();

    /**
      * @brief Draws toolbox and all available nodes.
      */
    void drawToolbox();

protected:
    QGraphicsScene scene;
    QList<RegexGraphicsNode* > availableNodes;
signals:

public slots:

};

#endif // TOOLBOXSCENECONTAINER_H
