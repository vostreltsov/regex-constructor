#include "regexabstractnode.h"
#include "regexnodeinput.h"
#include "regexnodeoutput.h"
#include "regexgraphicsnode.h"
#include "regexgraphicscontainernode.h"
#include "graphscene.h"
#include <QVector2D>
#include <QTimer>

RegexAbstractNode::RegexAbstractNode(QGraphicsItem *parent) :
    QGraphicsObject(parent),
    input(NULL),
    output(NULL),
    containerHover(NULL)
{
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges);
    setFlag(QGraphicsItem::ItemIgnoresParentOpacity);
    inToolbox = false;
}

RegexAbstractNode::~RegexAbstractNode()
{
    if (containerHover) {
        containerHover->animateHoverLeaved();
    }
    deactivateInput();
    deactivateOutput();
    if (parentItem() && parentItem()->type() == RegexGraphicsContainerNode::Type) {
        RegexGraphicsContainerNode* container
                = qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentItem());
        container->removeFromContainer(this);
    }
}

RegexAbstractNode *RegexAbstractNode::findBottomNode(QList<RegexAbstractNode *> list)
{
    RegexAbstractNode *bottom = list.first();
    foreach (RegexAbstractNode *node, list) {
        if (node->isAncestorOf(bottom))
            bottom = node;
    }
    return bottom;
}

QString RegexAbstractNode::generateRegex(bool *previousNodeGrouped)
{
    return ""; /* When regex is renerating after deletion of node this function can be called
                 because regex regeneration can be executed between child and parent destructors
                 calls. If you can suggest something more valid please do. */
}

bool RegexAbstractNode::isInToolbox() const
{
    return inToolbox;
}


bool RegexAbstractNode::isInputActive() const
{
    return input != NULL;
}

bool RegexAbstractNode::isOutputActive() const
{
    return output != NULL;
}

RegexNodeInput* RegexAbstractNode::getInput() const
{
    Q_ASSERT(isInputActive());
    return input;
}
RegexNodeOutput* RegexAbstractNode::getOutput() const
{
    Q_ASSERT(isOutputActive());
    return output;
}


void RegexAbstractNode::activateInput()
{
    if (input == NULL) {
        input = new RegexNodeInput(this);
    }
}

void RegexAbstractNode::activateOutput()
{
    if (output == NULL) {
        output = new RegexNodeOutput(this);
    }
}

void RegexAbstractNode::deactivateInput()
{
    if (input != NULL) {
        delete input;
        input = NULL;
    }
}

void RegexAbstractNode::deactivateOutput()
{
    if (output != NULL) {
        delete output;
        output = NULL;
    }
}

void RegexAbstractNode::ioStateChanged()
{
    GraphScene *graph = qobject_cast<GraphScene *>(scene());
    if (graph) {
        //graph->removeAutoConnections();
        graph->makeAutoConnections();
        graph->generateRegex();
        //graph->makeAutoConnections();
    }
}

int RegexAbstractNode::type() const
{
    return Type;
}

void RegexAbstractNode::shiftCollidingItems(QGraphicsItem *ignore)
{
    QList<QGraphicsItem *> collisions;
    collisions = collidingItems(Qt::IntersectsItemBoundingRect);
    if (collisions.size() > 0) {
        foreach (QGraphicsItem * item, collisions) {
            if ( (item->type() == RegexGraphicsNode::Type || item->type() == RegexGraphicsContainerNode::Type)
                 && parentItem() != item
                 && parentItem() == item->parentItem()
                 && item != ignore) {


                QVector2D direction(item->sceneBoundingRect().center() - sceneBoundingRect().center());
                direction.normalize();
                if (direction.isNull())
                    direction = QVector2D(1, 1);
                direction *= 6;
                if (!item->parentItem()) {
                    item->moveBy(direction.x(), direction.y());
                } else if ((item->sceneBoundingRect() & item->parentItem()->sceneBoundingRect()) == item->sceneBoundingRect()) {
                    item->moveBy(direction.x(), direction.y());
                }
                RegexGraphicsNode *graphicsItem = qgraphicsitem_cast<RegexGraphicsNode*>(item);
                if (graphicsItem)
                    QTimer::singleShot(20, graphicsItem, SLOT(shiftCollidingItems()));

                RegexGraphicsContainerNode *containerItem = qgraphicsitem_cast<RegexGraphicsContainerNode*>(item);
                if (containerItem)
                    QTimer::singleShot(20, containerItem, SLOT(shiftCollidingItems()));

            }
        }
    }
}

RegexGraphicsContainerNode * RegexAbstractNode::findPossibleContainer()
{
    RegexGraphicsContainerNode * container = NULL;
    QList<QGraphicsItem *> collisions = collidingItems(Qt::IntersectsItemBoundingRect);
    for (QList<QGraphicsItem *>::iterator item = collisions.begin(); item != collisions.end() && !container; ++item) {
        if ((*item)->type() == RegexGraphicsContainerNode::Type && !parentOf(*item)) {
            container = qgraphicsitem_cast<RegexGraphicsContainerNode *>(*item);
        }
    }
    return container;
}

bool RegexAbstractNode::changeContainer()
{
    RegexGraphicsContainerNode * container = findPossibleContainer();
    RegexGraphicsContainerNode* previousCointainer
            = qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentItem());
    if (container) {
        if (previousCointainer && previousCointainer != container)
            previousCointainer->removeFromContainer(this);
        return container->addToContainer(this);
    } else if(!container && previousCointainer) {
        return previousCointainer->removeFromContainer(this);
    }
    return false;
}

QVariant RegexAbstractNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (scene() && QGraphicsItem::ItemPositionChange == change) {
        QPointF newPos;
        if (parentItem())
            newPos = parentItem()->mapToScene(value.toPointF());
        else
            newPos = value.toPointF();
        QPointF newBottomRight = newPos + boundingRect().bottomRight();
        if (newPos.x() < 0)
            newPos.setX(0);
        else if (newBottomRight.x() > scene()->sceneRect().right())
            newPos.setX(scene()->sceneRect().right() - boundingRect().width());
        if (newPos.y() < 0)
            newPos.setY(0);
        else if (newBottomRight.y()  > scene()->sceneRect().bottom())
            newPos.setY(scene()->sceneRect().bottom() - boundingRect().height());
        if (parentItem())
            return qVariantFromValue(parentItem()->mapFromScene(newPos));
        else
            return qVariantFromValue(newPos);
    }
    return QGraphicsObject::itemChange(change, value);
}

void RegexAbstractNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
    QGraphicsObject::mousePressEvent(event);
}

void RegexAbstractNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    RegexGraphicsContainerNode *newContainer = NULL;
    newContainer = findPossibleContainer();
    if (newContainer != containerHover) {
        if (containerHover) {
            containerHover->animateHoverLeaved();
        }
        containerHover = newContainer;
        if (containerHover) {
            containerHover->animateHoverEntered();
        }
    }
    QGraphicsObject::mouseMoveEvent(event);
}

void RegexAbstractNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    changeContainer();
    if (containerHover) {
        containerHover->animateHoverLeaved();
        containerHover = NULL;
    }
    shiftCollidingItems();
    event->accept();
    QGraphicsObject::mouseReleaseEvent(event);
}


bool RegexAbstractNode::parentOf(QGraphicsItem* item)
{
    while (item->parentItem() != NULL) {
        item = item->parentItem();
        if (item == this)
            return true;
    }
    return false;
}

bool RegexAbstractNode::collidesWithItem(const QGraphicsItem * other, Qt::ItemSelectionMode mode) const
{
    if (mode == Qt::IntersectsItemBoundingRect) {
        QRectF otherRect = other->boundingRect();
        otherRect = mapFromItem(other, otherRect).boundingRect();
        bool result = !(otherRect & boundingRect()).isEmpty();
        return result;
    }
    return QGraphicsObject::collidesWithItem(other, mode);
}

bool RegexAbstractNode::hasProperties() const
{
    return true;
}

QList<RegexAbstractNode::PropertyPair> RegexAbstractNode::getProperties()
{
    return QList<PropertyPair>();
}

void RegexAbstractNode::setProperties(QList<RegexAbstractNode::PropertyPair> props)
{

}

void RegexAbstractNode::removeNode()
{
    if (parentItem() && parentItem()->type() == RegexGraphicsContainerNode::Type) {
        RegexGraphicsContainerNode* container
                = qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentItem());
        container->removeFromContainer(this);
    }
    deleteLater();
}

void RegexAbstractNode::removeByUser()
{
    GraphScene *sc = static_cast<GraphScene *>(scene());
    sc->createUndoAction();
    removeNode();
}
