#include "regexnodeiointerface.h"
#include "regexabstractnode.h"
#include "algorithms.h"
#include "regexgraphicscontainernode.h"

RegexNodeIOInterface::RegexNodeIOInterface(RegexAbstractNode *parent) :
    QGraphicsObject(parent),
    parentNode(parent),
    ioBoundingRect(-15, -15, 15, 15)
{
}



RegexNodeIOInterface::~RegexNodeIOInterface()
{
    prepareForRemove();
    clear();
}

bool RegexNodeIOInterface::addConnection(RegexNodeConnection* con)
{
    foreach (RegexNodeConnection* connection, connections) {
        if (connection->getSource() == con->getSource()
                && connection->getDestination() == con->getDestination())
            return false;
    }
    connections.append(con);
    return true;
}

void RegexNodeIOInterface::removeConnection(RegexNodeConnection* con)
{
    connections.removeOne(con);
}

RegexAbstractNode *RegexNodeIOInterface::getParentNode() const
{
    return parentNode;
}

void RegexNodeIOInterface::adjustPos()
{
    foreach (RegexNodeConnection *arc, connections) {
        arc->redraw();
    }
}

void RegexNodeIOInterface::clear()
{

    foreach(RegexNodeConnection* conn, connections) {
        delete conn;
    }
}


void RegexNodeIOInterface::prepareForRemove()
{
    parentNode = NULL;
}


size_t RegexNodeIOInterface::connectionsCount()
{
    return connections.size();
}

QList<RegexNodeConnection *> RegexNodeIOInterface::getConnections()
{
    return connections;
}

QList<ConnEndPair> RegexNodeIOInterface::getIncidentConnectionPairs()
{
    QList<ConnEndPair> pairs;
    foreach (RegexNodeConnection *arc, connections) {
        pairs.append(qMakePair(arc, arc->getSource() == getParentNode() ?
                                   arc->getDestination()
                                 : arc->getSource()));
    }
    return pairs;
}

QRectF RegexNodeIOInterface::boundingRect() const
{
    return ioBoundingRect;
}
