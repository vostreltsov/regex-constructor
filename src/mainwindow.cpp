#include "mainwindow.h"
#include <QUndoStack>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    testingWindow = new TestingWindow();
    toolbox = new ToolboxSceneContainer(parent);
    RegexGraphicsNodeFactory::setMainWindow(this); /** @badcode  */
    scene = new GraphScene(parent);

    /*QMap<QString, QString> posix = RegexGraphicsLeafCharset::getPosixMap();
    for (QMap<QString, QString>::const_iterator iter = posix.constBegin(); iter != posix.constEnd(); ++iter) {
        ui->comboBoxCharsetPosix->addItem(iter.key());
    }*/
    ui->comboBoxAssertion->addItem(ASSERT_EMPTY);
    ui->comboBoxAssertion->addItem(ASSERT_CIRCUMFLEX);
    ui->comboBoxAssertion->addItem(ASSERT_DOLLAR);
    ui->comboBoxAssertion->addItem(ASSERT_BOUND);
    ui->comboBoxAssertion->addItem(ASSERT_NOT_BOUND);

    ui->btnCharsetAddPosix->setVisible(false);
    ui->comboBoxCharsetPosix->setVisible(false);
    ui->cbCharsetPosixNegative->setVisible(false);

    ui->graphicsViewGraph->setScene(scene);
    ui->graphicsViewGraph->centerOn(0, scene->sceneRect().center().y());
    ui->graphicsViewToolbox->setScene(toolbox->getScene());
    ui->graphicsViewToolbox->setMaximumWidth(toolbox->getScene()->sceneRect().width());

    // Connects for the scene.
    connect(scene, SIGNAL(hidePropertyEditor()), this, SLOT(hidePropertyEditor()));
    connect(scene, SIGNAL(regexChanged(QString, bool)), this, SLOT(refreshRegex(QString, bool)));
    connect(scene, SIGNAL(sceneChanged()), this, SLOT(sceneChanged()));
    connect(scene, SIGNAL(regexChanged(QString, bool)), this, SLOT(sceneChanged()));
    connect(scene->getUndoStack(), SIGNAL(canUndoChanged(bool)), ui->actionUndo, SLOT(setEnabled(bool)));
    connect(scene->getUndoStack(), SIGNAL(canRedoChanged(bool)), ui->actionRedo, SLOT(setEnabled(bool)));

    // Connects for line edits changes.
    connect(ui->lineEditCharsetSingle, SIGNAL(editingFinished()), this, SLOT(lineEdit_editingFinished()));
    connect(ui->lineEditFiniteQuantLeft, SIGNAL(editingFinished()), this, SLOT(lineEdit_editingFinished()));
    connect(ui->lineEditFiniteQuantRight, SIGNAL(editingFinished()), this, SLOT(lineEdit_editingFinished()));
    connect(ui->lineEditInfiniteQuantLeft, SIGNAL(editingFinished()), this, SLOT(lineEdit_editingFinished()));
    connect(ui->lineEditString, SIGNAL(editingFinished()), this, SLOT(lineEdit_editingFinished()));

    // Connects for checkboxes.
    connect(ui->cbCharsetNegative, SIGNAL(stateChanged(int)), this, SLOT(checkBox_stateChanged(int)));
    //connect(ui->cbCharsetPosixNegative, SIGNAL(stateChanged(int)), this, SLOT(checkBox_stateChanged(int)));

    // Connects for comboboxes.
    connect(ui->comboBoxAssertion, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBox_currentIndexChanged(int)));
    //connect(ui->comboBoxCharsetPosix, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBox_currentIndexChanged(int)));

    // Connects for menus
    connect(ui->actionNew, SIGNAL(triggered(bool)), this, SLOT(actionNew_triggered()));
    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(actionOpen_triggered()));
    connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(actionSave_triggered()));
    connect(ui->actionSaveAs, SIGNAL(triggered(bool)), this, SLOT(actionSaveAs_triggered()));
    connect(ui->actionExit, SIGNAL(triggered(bool)), this, SLOT(actionExit_triggered()));
    connect(ui->actionUndo, SIGNAL(triggered(bool)), this, SLOT(actionUndo_triggered()));
    connect(ui->actionRedo, SIGNAL(triggered(bool)), this, SLOT(actionRedo_triggered()));
    connect(ui->actionCut, SIGNAL(triggered(bool)), this, SLOT(actionCut_triggered()));
    connect(ui->actionCopy, SIGNAL(triggered(bool)), this, SLOT(actionCopy_triggered()));
    connect(ui->actionPaste, SIGNAL(triggered(bool)), this, SLOT(actionPaste_triggered()));
    connect(ui->actionSelectAll, SIGNAL(triggered(bool)), this, SLOT(actionSelectAll_triggered()));
    connect(ui->actionHelp, SIGNAL(triggered(bool)), this, SLOT(actionHelp_triggered()));
    connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(actionAbout_triggered()));
    connect(ui->actionExportImage, SIGNAL(triggered(bool)), this, SLOT(actionExport_triggered()));

    // Connects for misc buttons.
    connect(ui->btnCharsetAddRange, SIGNAL(clicked(bool)), this, SLOT(btnCharsetAddRange_clicked()));
    connect(ui->btnCharsetAddPosix, SIGNAL(clicked(bool)), this, SLOT(btnCharsetAddPosix_clicked()));
    connect(ui->btnCharsetRemoveSelected, SIGNAL(clicked(bool)), this, SLOT(btnCharsetRemoveSelected_clicked()));

    connect(ui->btnHidePropsCharset, SIGNAL(clicked(bool)), this, SLOT(btnHideProperties_clicked()));
    connect(ui->btnHidePropsAssertion, SIGNAL(clicked(bool)), this, SLOT(btnHideProperties_clicked()));
    connect(ui->btnHidePropsFiniteQuant, SIGNAL(clicked(bool)), this, SLOT(btnHideProperties_clicked()));
    connect(ui->btnHidePropsInfiniteQuant, SIGNAL(clicked(bool)), this, SLOT(btnHideProperties_clicked()));
    connect(ui->btnHidePropsString, SIGNAL(clicked(bool)), this, SLOT(btnHideProperties_clicked()));

    // Connect the BIG RED BUTTON.
    connect(ui->btnTestRegex, SIGNAL(clicked(bool)), this, SLOT(btnTestRegex_clicked()));

    toolbox->drawToolbox();

    validatorInt = new QRegExpValidator(QRegExp("\\d+"), parent);
    ui->lineEditFiniteQuantLeft->setValidator(validatorInt);
    ui->lineEditFiniteQuantRight->setValidator(validatorInt);
    ui->lineEditInfiniteQuantLeft->setValidator(validatorInt);

    //validatorChar = new QRegExpValidator(QRegExp(".|\\\\x\\{[\\da-fA-F]+\\}|\\\\x[\\da-fA-F]?[\\da-fA-F]?"), parent);
    validatorChar = new QRegExpValidator(QRegExp(".|\\\\x[\\da-fA-F]?[\\da-fA-F]?"), parent);
    ui->lineEditCharsetRangeStart->setValidator(validatorChar);
    ui->lineEditCharsetRangeEnd->setValidator(validatorChar);

    editingNode = NULL;

    hidePropertyEditor();
}

MainWindow::~MainWindow()
{
    delete validatorInt;
    delete validatorChar;
    delete toolbox;
    delete scene;
    delete testingWindow;
    delete ui;
}

void MainWindow::lineEdit_editingFinished()
{
    switch (ui->stackedWidget->currentIndex()) {
    case 0:
        // Handle charset properties.
        savePropsCharset();
        break;
    case 1:
        // Handle assertion properties.
        break;
    case 2:
        // Handle finite quantifier properties.
        savePropsFiniteQuant();
        break;
    case 3:
        // Handle infinite quantifier properties.
        savePropsInfiniteQuant();
        break;
    case 4:
        // Handle string properties.
        savePropsString();
        break;
    default:
        break;
    }
}

void MainWindow::checkBox_stateChanged(int state)
{
    // Handle charset properties.
    ui->labelCharsetSingle->setText(state ? LAB_CHARSET_DISALLOWED : LAB_CHARSET_ALLOWED);
    savePropsCharset();
}

void MainWindow::comboBox_currentIndexChanged(int index)
{
    // Handle assertion properties.
    savePropsAssertion();
}

void MainWindow::listWidgetRanges_itemSetChanged()
{
    // Handle charset properties.
    savePropsCharset();
}

void MainWindow::showPropertyEditor(RegexGraphicsNode * node)
{
    editingNode = node;
    RegexGraphicsNode::RegexNodeType type = node->nodeType();

    switch (type) {
    case RegexGraphicsNode::TYPE_LEAF_CHARSET:
        ui->stackedWidget->setCurrentIndex(0);
        loadPropsCharset((RegexGraphicsLeafCharset *)node);
        ui->stackedWidget->setVisible(true);
        break;
    case RegexGraphicsNode::TYPE_LEAF_ASSERT:
        ui->stackedWidget->setCurrentIndex(1);
        loadPropsAssert((RegexGraphicsLeafAssert *)node);
        ui->stackedWidget->setVisible(true);
        break;
    case RegexGraphicsNode::TYPE_LEAF_STRING:
        ui->stackedWidget->setCurrentIndex(4);
        loadPropsString((RegexGraphicsLeafString *)node);
        ui->stackedWidget->setVisible(true);
        break;
    case RegexGraphicsNode::TYPE_NODE_FINITE_QUANT:
        ui->stackedWidget->setCurrentIndex(2);
        loadPropsFiniteQuant((RegexGraphicsFiniteQuant *)node);
        ui->stackedWidget->setVisible(true);
        break;
    case RegexGraphicsNode::TYPE_NODE_INFINITE_QUANT:
        ui->stackedWidget->setCurrentIndex(3);
        loadPropsInfiniteQuant((RegexGraphicsInfiniteQuant *)node);
        ui->stackedWidget->setVisible(true);
        break;
    default:
        ui->stackedWidget->setVisible(false);
        editingNode = NULL;
        break;
    }
}

void MainWindow::showPropertyEditor(RegexNodeConnection * connection)
{
    editingNode = connection->getPayload();
    editingConnection = connection;
    ui->stackedWidget->setCurrentIndex(1);
    loadPropsAssert((RegexGraphicsLeafAssert *)editingNode);
    ui->stackedWidget->setVisible(true);
}

void MainWindow::hidePropertyEditor()
{
    editingNode = NULL;
    editingConnection = NULL;
    ui->stackedWidget->setVisible(false);
}

void MainWindow::refreshRegex(QString regex, bool ok)
{
    ui->lineEditResultingRegex->setText(regex);
    ui->btnTestRegex->setEnabled(ok);
}

void MainWindow::sceneChanged()
{
    setWindowModified(true);
}

void MainWindow::actionNew_triggered()
{
    switch (youSure()) {
    case QMessageBox::Yes:
        actionSave_triggered();
        break;
    case QMessageBox::No:
        break;
    default:
        return;
    }
    scene->clearNodes();
    scene->getUndoStack()->clear();
    setWindowFilePath(UNTITLED_FILE_NAME);
    setWindowModified(false);
}

void MainWindow::actionOpen_triggered()
{
    switch (youSure()) {
    case QMessageBox::Yes:
        actionSave_triggered();
        break;
    case QMessageBox::No:
        break;
    default:
        return;
    }
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    "Open File",
                                                    "",
                                                    "RegexGraph (*.rxg);;All files (*.*)");
    if (fileName.length() != 0) {
        scene->clearNodes();
        setWindowFilePath(fileName);
        GraphIO::loadFromFile(fileName, scene);
        setWindowModified(false);
    }
}

void MainWindow::actionSave_triggered()
{
    QString fileName(windowFilePath());
    if (!fileName.isEmpty() && fileName != UNTITLED_FILE_NAME) {
        GraphIO::saveToFile(fileName, scene);
        setWindowModified(false);
    } else {
        actionSaveAs_triggered();
    }
}

void MainWindow::actionSaveAs_triggered()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    "Save File",
                                                    "",
                                                    "RegexGraph (*.rxg);;All files (*.*)",
                                                    &selectedFilter);
    if(fileName.length() != 0){
        if (selectedFilter == "RegexGraph (*.rxg)" && !fileName.contains(".rxg"))
            fileName.append(".rxg");
        GraphIO::saveToFile(fileName, scene);
        setWindowFilePath(fileName);
        setWindowModified(false);
    }

}

void MainWindow::actionExit_triggered()
{
    close();
}

void MainWindow::actionUndo_triggered()
{
    if (scene->getUndoStack()->canUndo()) {
        scene->getUndoStack()->undo();
        hidePropertyEditor();
    }
}

void MainWindow::actionRedo_triggered()
{
    if (scene->getUndoStack()->canRedo()) {
        scene->getUndoStack()->redo();
        hidePropertyEditor();
    }
}

void MainWindow::actionCut_triggered()
{
    scene->copySelection();
    foreach (QGraphicsItem* item, scene->selectedItems()) {
        if (item->toGraphicsObject())
            item->toGraphicsObject()->deleteLater();
    }
}

void MainWindow::actionCopy_triggered()
{
    scene->copySelection();
}

void MainWindow::actionPaste_triggered()
{
    scene->paste(ui->graphicsViewGraph->mapToScene(ui->graphicsViewGraph->viewport()->rect().topLeft()));
}

void MainWindow::actionSelectAll_triggered()
{
    scene->selectAll();
}

void MainWindow::actionHelp_triggered()
{
    QMessageBox::information(NULL, QString("Hey dude"), QString("You've just got some help, haven't ya?"));
}

void MainWindow::actionAbout_triggered()
{
    QMessageBox::information(NULL, QString("About"), QString("<p align='center'>This is an easy (at least it tries to be) regex constructor.<br>") +
                                                     QString("You can do with this app whatever you want, even use its source for toilet paper.<br>") +
                                                     QString("Authors: Streltsov Valeriy, Mednikov Alexey, Goremykin Michael.<br>") +
                                                     QString("Volgograd State Technical University, 2012<p>"));
}

void MainWindow::actionExport_triggered()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    "Save File",
                                                    "",
                                                    "PNG (*.png);;JPG (*.jpg);;BMP (*.bmp)",
                                                    &selectedFilter);
    if (selectedFilter == "PNG (*.png)") {
        if (!fileName.endsWith(".png"))
            fileName.append(".png");
    } else if (selectedFilter == "JPG (*.jpg)") {
        if (!fileName.endsWith(".jpg"))
            fileName.append(".jpg");
    } else if (selectedFilter == "BMP (*.bmp)") {
        if (!fileName.endsWith(".bmp"))
            fileName.append(".bmp");
    }
    QPixmap pixmap = scene->getImage();
    pixmap.save(fileName);
}

void MainWindow::btnHideProperties_clicked()
{
    hidePropertyEditor();
}

void MainWindow::btnTestRegex_clicked()
{
    testingWindow->getUi()->lineEditTestingRegex->setText(ui->lineEditResultingRegex->text());
    testingWindow->show();
}

QMessageBox::StandardButton MainWindow::youSure()
{
    if (isWindowModified()) {
        return QMessageBox::question(this, "Regex modified", "Do you want to save changes?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Yes);
    } else {
        return QMessageBox::No;
    }
}

void MainWindow::savePropsCharset()
{
    if (editingNode == NULL) {
        return;
    }
    scene->createUndoAction();
    RegexGraphicsLeafCharset * node = (RegexGraphicsLeafCharset *)editingNode;
    QStringList ranges;
    for (int i = 0; i < ui->listWidgetCharsetRanges->count(); i++)
        ranges.append(ui->listWidgetCharsetRanges->item(i)->text());
    node->setEverything(ui->cbCharsetNegative->isChecked(), ui->lineEditCharsetSingle->text(), ranges, QStringList());
}

void MainWindow::savePropsAssertion()
{
    if (editingConnection == NULL) {
        return;
    }
    scene->createUndoAction();

    if (ui->comboBoxAssertion->currentText() == ASSERT_EMPTY) {
        //delete editingNode;   // performed by editingConnection->setPayload(node);
        editingNode = NULL;
    } else {
        if (!editingNode) {
            editingNode = RegexGraphicsNodeFactory::get(RegexGraphicsNode::TYPE_LEAF_ASSERT);
        }
    }

    RegexGraphicsLeafAssert * node = (RegexGraphicsLeafAssert *)editingNode;
    if (node) {
        node->setAssertion(ui->comboBoxAssertion->currentText());
        node->updateText();
    }
    editingConnection->setPayload(node);
    editingConnection->redraw();
    scene->generateRegex();
}

void MainWindow::savePropsFiniteQuant()
{
    if (editingNode == NULL) {
        return;
    }
    if (ui->lineEditFiniteQuantLeft->text().isEmpty()) {
        //QMessageBox::warning(NULL, QString("Empty left border"), QString("You should specify the left border."));
        return;
    }
    if (ui->lineEditFiniteQuantRight->text().isEmpty()) {
        //QMessageBox::warning(NULL, QString("Empty right border"), QString("You should specify the right border."));
        return;
    }

    QString left = ui->lineEditFiniteQuantLeft->text();
    QString right = ui->lineEditFiniteQuantRight->text();

    if (left.toInt() > right.toInt()) {
        QMessageBox::warning(NULL, QString("Incorrect quantifier borders"), QString("The left border is greater than the right one."));
        return;
    }

    scene->createUndoAction();
    RegexGraphicsFiniteQuant * node = (RegexGraphicsFiniteQuant *)editingNode;
    node->setLeftBorder(left);
    node->setRigntBorder(right);
}

void MainWindow::savePropsInfiniteQuant()
{
    if (editingNode == NULL) {
        return;
    }
    QString left = ui->lineEditInfiniteQuantLeft->text();

    if (left.isEmpty()) {
        //QMessageBox::warning(NULL, QString("Empty left border"), QString("You should specify the left border."));
        return;
    }

    scene->createUndoAction();
    RegexGraphicsInfiniteQuant * node = (RegexGraphicsInfiniteQuant *)editingNode;
    node->setLeftBorder(left);
}

void MainWindow::savePropsString()
{
    if (editingNode == NULL) {
        return;
    }
    scene->createUndoAction();
    RegexGraphicsLeafString * node = (RegexGraphicsLeafString *)editingNode;
    node->setText(ui->lineEditString->text());
}

void MainWindow::loadPropsCharset(RegexGraphicsLeafCharset * node)
{
    ui->cbCharsetNegative->setChecked(node->isNegative());
    ui->lineEditCharsetSingle->setText(node->getSingleChars());
    ui->listWidgetCharsetRanges->clear();
    ui->listWidgetCharsetRanges->addItems(node->getRanges());
    ui->comboBoxCharsetPosix->setCurrentIndex(0);
}

void MainWindow::loadPropsAssert(RegexGraphicsLeafAssert * node)
{

    if (node) {
        bool done = false;
        for (int i = 0; !done && i < ui->comboBoxAssertion->count(); i++) {
            if (node->getText() == ui->comboBoxAssertion->itemText(i)) {
                ui->comboBoxAssertion->setCurrentIndex(i);
                done = true;
            }
        }
    } else {
        ui->comboBoxAssertion->setCurrentIndex(0);
    }
}

void MainWindow::loadPropsFiniteQuant(RegexGraphicsFiniteQuant * node)
{
    QString left = node->getLeftBorder();
    if (left == "x") {
        left = "0";
    }
    QString right = node->getRightBorder();
    if (right == "y") {
        right = "0";
    }
    ui->lineEditFiniteQuantLeft->setText(left);
    ui->lineEditFiniteQuantRight->setText(right);
}

void MainWindow::loadPropsInfiniteQuant(RegexGraphicsInfiniteQuant * node)
{
    QString left = node->getLeftBorder();
    if (left == "x") {
        left = "0";
    }
    ui->lineEditInfiniteQuantLeft->setText(left);
}

void MainWindow::loadPropsString(RegexGraphicsLeafString * node)
{
    ui->lineEditString->setText(node->getText());
}

void MainWindow::btnCharsetAddRange_clicked()
{
    QString text = ui->lineEditCharsetRangeStart->text() + QString("-") + ui->lineEditCharsetRangeEnd->text();
    bool exists = false;
    for (int i = 0; !exists && i < ui->listWidgetCharsetRanges->count(); i++)
        exists |= ui->listWidgetCharsetRanges->item(i)->text() == text;
    if (!exists) {
        ui->listWidgetCharsetRanges->addItem(new QListWidgetItem(text));
        listWidgetRanges_itemSetChanged();
    }
}

void MainWindow::btnCharsetAddPosix_clicked()
{
    /*QString text = ui->comboBoxCharsetPosix->currentText();
    if (ui->cbCharsetPosixNegative->isChecked())
        text = "not " + text;
    bool exists = false;
    for (int i = 0; !exists && i < ui->listWidgetCharsetFlags->count(); i++)
        exists |= ui->listWidgetCharsetFlags->item(i)->text() == text;
    if (!exists)
        ui->listWidgetCharsetFlags->addItem(new QListWidgetItem(text));*/
}

void MainWindow::btnCharsetRemoveSelected_clicked()
{
    int index = ui->listWidgetCharsetRanges->currentRow();
    if (index >= 0) {
        delete ui->listWidgetCharsetRanges->takeItem(index);
        listWidgetRanges_itemSetChanged();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    switch (youSure()) {
    case QMessageBox::Yes:
        actionSave_triggered();
        break;
    case QMessageBox::No:
        break;
    default:
        event->ignore();
        break;
    }
}
