#ifndef REGEXGRAPHICSOPERANDS_H
#define REGEXGRAPHICSOPERANDS_H

#include "regexgraphicsnode.h"
#include "translation.h"

class RegexGraphicsLeafCharset : public RegexGraphicsNode
{
protected:
    /** @var Is this charset negative. */
    bool negative;
    /** @var Enumerable characters accepted by this charset. */
    QString singleChars;
    /** @var Ranges of chararcters. */
    QStringList ranges;
    /** @var Array of POSIX classes. */
    QStringList posixClasses;

    void updateText();

public:
    explicit RegexGraphicsLeafCharset(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    bool isNegative() const;
    QString getSingleChars() const;
    QStringList getRanges() const;
    QStringList getPosixClasses() const;
    void setNegative(bool value);
    void setSingleChars(const QString & value);
    void setRanges(const QStringList & value);
    void setPosixClasses(const QStringList & value);
    void setEverything(bool negative, const QString & singleChars, const QStringList & ranges, const QStringList & posix);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    static QMap<QString, QString> getPosixMap();
    QList<PropertyPair> getProperties();
    void setProperties(QList<PropertyPair> props);
};

class RegexGraphicsLeafAssert : public RegexGraphicsNode
{
public:
    explicit RegexGraphicsLeafAssert(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);
    void setAssertion(QString value);
    QString getAssertion() const;
    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    void updateText();
    QList<PropertyPair> getProperties();
    void setProperties(QList<PropertyPair> props);
protected:
    QString assertion;
};

class RegexGraphicsLeafDot : public RegexGraphicsNode
{
public:
    explicit RegexGraphicsLeafDot(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    bool hasProperties() const;
};

class RegexGraphicsLeafString : public RegexGraphicsNode
{
public:
    explicit RegexGraphicsLeafString(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);
    void setText(QString value);
    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    QList<PropertyPair> getProperties();
    void setProperties(QList<PropertyPair> props);
};

#endif // REGEXGRAPHICSOPERANDS_H
