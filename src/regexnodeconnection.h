#ifndef REGEXGRAPHICSNODECONNECTION_H
#define REGEXGRAPHICSNODECONNECTION_H

#include <QObject>
#include <QGraphicsPathItem>
#include <QGraphicsScene>
#include <QPainterPath>
#include "regexgraphicsconnectionarc.h"

class RegexAbstractNode;
class RegexNodeInput;
class RegexNodeOutput;
class RegexGraphicsNode;
class GraphScene;

class RegexNodeConnection : public QObject
{
    Q_OBJECT

protected:
    /** @var Pointer to the origin node.*/
    RegexAbstractNode* from;

    /** @var Pointer to the destination node.*/
    RegexAbstractNode* to;

    /** @var PathItem which represents connection on graphics scene. */
    RegexGraphicsConnectionArc* pathItem;
    /** @var Endpoint for a connection arc which will be used fro drawing in case of null destination node.*/
    QPointF endpoint;
    /** @var Text, that describes payload contents near the arc. */
    QGraphicsTextItem *payloadText;

    /** @var Additional regex part that will be generated from this arc. */
    RegexGraphicsNode *payload;

    RegexNodeOutput* sourceOut;
    RegexNodeInput* destIn;
    bool validity;
    bool fake;
    GraphScene *arcScene;
public:

    /** @brief Creates a new connection, which can have only origin node or both origin and destination nodes.*/
    explicit RegexNodeConnection(RegexNodeOutput* _from, RegexNodeInput* _to = NULL, QObject *parent = 0);
    ~RegexNodeConnection();
    static RegexNodeConnection* createConnection(RegexAbstractNode *from, RegexAbstractNode *to);

    /** @brief Returns pointer to the source node */
    RegexAbstractNode* getSource() const;

    /** @brief Returns pointer to the destination node */
    RegexAbstractNode* getDestination() const;

    /** @brief Returns pointer to the source output */
    RegexNodeOutput* getSourceOutput() const;

    /** @brief Returns pointer to the destination input */
    RegexNodeInput* getDestinationInput() const;

    void setValid(bool valid);
    bool isValid();

    void setFake(bool fake);
    bool isFake();

    void setPayload(RegexGraphicsNode *node);
    RegexGraphicsNode * getPayload();
    QString generatePayloadRegex(bool *previousNodeGrouped);



protected:
    /** @brief Returns a painter path with bezier curve from origin to target/endpoint. */
    QPainterPath constructArc();

signals:
    void showPropertyEditor(RegexNodeConnection *connection);
public slots:

    /** @brief Constructs and draws new arc, use in case of origin or target postion change. */
    void redraw();

    /** @brief Sets endpoint. */
    void setEndpoint(const QPointF &point);

    /** @brief Sets destination node if it's NULL. */
    void setDestination(RegexNodeInput *dest);

    /** @brief Removes connection and remembers graph state for undo. */
    void removeByUser();

    void showPayloadPropertyEditor();

    void showArcPropertyEditor();
};

#endif // REGEXGRAPHICSNODECONNECTION_H
