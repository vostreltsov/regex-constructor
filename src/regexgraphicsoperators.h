#ifndef REGEXGRAPHICSOPERATORS_H
#define REGEXGRAPHICSOPERATORS_H

#include "regexgraphicsnode.h"
#include "regexgraphicscontainernode.h"

/**
 * @brief The RegexOperator class represents an abstract regex operator.
 */
class RegexGraphicsOperator : public RegexGraphicsContainerNode
{
protected:
    /** @var An array of operands of this operator. */
    QVector<RegexGraphicsNode *> operands;

    /**
     * @brief Updates label of the node.
     */
    virtual void updateLabel();

public:
    explicit RegexGraphicsOperator(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);
};

class RegexGraphicsFiniteQuant : public RegexGraphicsOperator
{
protected:
    QString leftBorder;
    QString rightBorder;
    void updateLabel();

public:
    explicit RegexGraphicsFiniteQuant(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    QString getLeftBorder() const;
    QString getRightBorder() const;
    void setLeftBorder(const QString & value);
    void setRigntBorder(const QString & value);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    QList<PropertyPair> getProperties();
    void setProperties(QList<PropertyPair> props);
};

class RegexGraphicsInfiniteQuant : public RegexGraphicsOperator
{
protected:
    QString leftBorder;
    void updateLabel();

public:
    explicit RegexGraphicsInfiniteQuant(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    QString getLeftBorder() const;
    void setLeftBorder(const QString & value);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    QList<PropertyPair> getProperties();
    void setProperties(QList<PropertyPair> props);
};

class RegexGraphicsSubpatt : public RegexGraphicsOperator
{
protected:
    void updateLabel();

public:
    explicit RegexGraphicsSubpatt(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    bool hasProperties() const;
};

class RegexGraphicsGroup : public RegexGraphicsOperator
{
protected:
    void updateLabel();

public:
    explicit RegexGraphicsGroup(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    RegexNodeType nodeType() const;
    QString generateRegex(bool *previousNodeGrouped);
    bool hasProperties() const;
};

#endif // REGEXGRAPHICSOPERATORS_H
