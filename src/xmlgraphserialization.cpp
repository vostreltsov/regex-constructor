#include "xmlgraphserialization.h"
#include "regexabstractnode.h"
#include "regexgraphicsnodefactory.h"

#include <QTimer>
XMLGraphSerialization::XMLGraphSerialization(QObject *parent){

}

void XMLGraphSerialization::startSerialization(QXmlStreamWriter &writer)
{
    writer.writeStartDocument();
    writer.writeStartElement("Graph");
}

void XMLGraphSerialization::endSerialization(QXmlStreamWriter &writer)
{
    writer.writeEndElement(); // Graph
    writer.writeEndDocument();
}

QString XMLGraphSerialization::serializeGraph(GraphScene *scene){
    QList<RegexAbstractNode *>  graph =  scene->getRegexNodes();

    QString serializedValue;
    QXmlStreamWriter writer(&serializedValue);
    writer.setAutoFormatting(true);
    startSerialization(writer);
    writer.writeEmptyElement("Scene");
    writer.writeAttribute("width", QString::number(scene->sceneRect().width()));
    writer.writeAttribute("height", QString::number(scene->sceneRect().height()));
    serializeNodeList(graph, writer);
    // Scene connections
    writer.writeStartElement("SceneInputEdges");
    QList<RegexNodeConnection* > nodeConnections = scene->getSceneInput()->getOutput()->getConnections();
    foreach (RegexNodeConnection* connection, nodeConnections) {
        serializeEdge(connection, writer);
    }
    writer.writeEndElement(); // SceneInputEdges
    endSerialization(writer);
    return serializedValue;
}

void XMLGraphSerialization::serializeNodeList(const QList<RegexAbstractNode *> &graph, QXmlStreamWriter &writer)
{
    writer.writeStartElement("Nodes");
    QList<RegexAbstractNode *>  nodes = GraphScene::findBottomLevel<RegexAbstractNode>(graph);
    foreach (RegexAbstractNode *node, nodes) {
        //if (node->parentItem() == 0) { // We need only top-level items, other items will be serialized by containers
            serializeNode(node, writer);
        //}
    }
    writer.writeEndElement(); // Nodes
}

void XMLGraphSerialization::serializeNode(RegexAbstractNode *node, QXmlStreamWriter &writer)
{

    RegexGraphicsNode *castedNode = static_cast<RegexGraphicsNode *>(node);
    RegexGraphicsNode::RegexNodeType nodeType = castedNode->nodeType();
    writer.writeComment(castedNode->getLabel());
    writer.writeStartElement("Node");
    writer.writeAttribute("id", QString::number(quintptr(node)));
    writer.writeAttribute("type", QString::number(nodeType));
    writer.writeAttribute("pos_x", QString::number(node->pos().x()));
    writer.writeAttribute("pos_y", QString::number(node->pos().y()));
    writer.writeAttribute("width", QString::number(node->boundingRect().width()));
    writer.writeAttribute("height", QString::number(node->boundingRect().height()));
    if (node->hasProperties()) {
        writer.writeStartElement("Properties");
        QList<RegexAbstractNode::PropertyPair> props = node->getProperties();
        foreach (RegexAbstractNode::PropertyPair pair, props) {
            writer.writeTextElement(pair.first, QString("'") + pair.second + "'");
        }
        writer.writeEndElement(); // Properties
    }
    if (node->type() == RegexGraphicsContainerNode::Type) { // Container serializes all contained nodes
        writer.writeStartElement("Contains");
        RegexGraphicsContainerNode *container = static_cast<RegexGraphicsContainerNode *>(node);
        foreach (RegexAbstractNode *in, container->getContainedNodes()) {
            serializeNode(in, writer);
        }
        writer.writeEndElement(); // Contains
        if (container->getFakeInputNode()->isOutputActive()) {
            writer.writeStartElement("InnerOutput");
            foreach (RegexNodeConnection *in, container->getFakeInputNode()->getOutput()->getConnections()) {
                serializeEdge(in, writer);
            }
            writer.writeEndElement(); // InnerOutput
        }
    }
    if (node->isInputActive()) {
        writer.writeStartElement("Input");
        foreach (RegexNodeConnection *in, node->getInput()->getConnections()) {
            serializeEdge(in, writer);
        }
        writer.writeEndElement(); // Input
    }
    if (node->isOutputActive()) {
        writer.writeStartElement("Output");
        foreach (RegexNodeConnection *in, node->getOutput()->getConnections()) {
            serializeEdge(in, writer);
        }
        writer.writeEndElement(); // Output
    }
    writer.writeEndElement(); // Node
}

void XMLGraphSerialization::serializeEdge(RegexNodeConnection* in, QXmlStreamWriter &writer)
{
    writer.writeStartElement("Edge");
    writer.writeAttribute("from", QString::number(quintptr(in->getSource())));
    writer.writeAttribute("to", QString::number(quintptr(in->getDestination())));
    writer.writeAttribute("fake", QString::number(in->isFake()));
    if (in->getPayload()) {
        writer.writeStartElement("Payload");
        serializeNode(in->getPayload(), writer);
        writer.writeEndElement(); // Payload
    }
    writer.writeEndElement(); // Edge

}

void XMLGraphSerialization::addToScene(RegexGraphicsNode *node, GraphScene *graph)
{
    graph->addItem(node);
    connect(node, SIGNAL(destroyed(QObject*)), graph, SLOT(removeRegexNodeFromContainer(QObject*)));
    graph->addNodeToContainer(node);
}

RegexGraphicsNode* XMLGraphSerialization::deserealizeNode(QDomNode domNode, QHash<QString, RegexGraphicsNode *> &lookup, GraphScene *scene, bool addToLookup)
{
    if (domNode.nodeName() != "Node")
        return NULL;
    QDomNamedNodeMap attribs = domNode.attributes();
    QString id = attribs.namedItem("id").toAttr().value();
    int type = attribs.namedItem("type").toAttr().value().toInt();
    QPointF pos(attribs.namedItem("pos_x").toAttr().value().toDouble(), attribs.namedItem("pos_y").toAttr().value().toDouble());
    QRectF size(0, 0, attribs.namedItem("width").toAttr().value().toDouble(), attribs.namedItem("height").toAttr().value().toDouble());

    RegexGraphicsNode* regexNode = RegexGraphicsNodeFactory::get(type);
    regexNode->setPos(pos);
    regexNode->changeBoundingRect(size);
    if (addToLookup)
        lookup.insert(id, regexNode);
    if (scene)
        addToScene(regexNode, scene);
    QDomElement nodeElement = domNode.toElement();
    QDomElement properties = domNode.firstChildElement("Properties");
    if (!properties.isNull()) {
        QList<RegexAbstractNode::PropertyPair> exportProps;
        QDomNodeList propList = properties.childNodes();
        for (size_t i = 0; i < propList.length(); ++i) {
            QDomNode prop = propList.at(i);
            QString propText = prop.firstChild().toText().data();
            propText = propText.mid(1, propText.length() - 2);
            exportProps.append(qMakePair(prop.nodeName(), propText));
        }
        regexNode->setProperties(exportProps);
    }
    QDomElement contained = domNode.firstChildElement("Contains");
    if (!contained.isNull()) { // Container
        RegexGraphicsContainerNode *container = static_cast<RegexGraphicsContainerNode *>(regexNode);
        QDomNodeList nodeList = contained.childNodes();
        for (size_t i = 0; i < nodeList.length(); ++i) {
            if (nodeList.at(i).isElement()) {
                RegexGraphicsNode* child = deserealizeNode(nodeList.at(i), lookup, scene);
                container->forceAddToContainer(child);
            }
        }
        QDomElement innerOutputEdges = domNode.firstChildElement("InnerOutput");
        if (!innerOutputEdges.isNull()) {
            QDomNodeList edgeList = innerOutputEdges.elementsByTagName("Edge");
            deserealizeEdges(edgeList, lookup, container->getFakeInputNode());
        }
    }
    return regexNode;
}

void XMLGraphSerialization::deserealizeEdges(QDomNodeList edgeList, QHash<QString, RegexGraphicsNode *> &lookup, RegexAbstractNode *overrideSource)
{
    for (size_t i = 0; i < edgeList.length(); ++i) {
        QDomNode edgeNode = edgeList.at(i);
        QDomNamedNodeMap edgeAttr = edgeNode.attributes();
        QString to = edgeAttr.namedItem("to").toAttr().value();
        if (overrideSource == NULL) {
            QString from = edgeAttr.namedItem("from").toAttr().value();
            overrideSource = lookup.value(from);
        }
        RegexAbstractNode *toNode = lookup.value(to);
        if (overrideSource != NULL && toNode != NULL) {
            bool fakeEdge = edgeAttr.namedItem("fake").toAttr().value().toInt();
            if (fakeEdge) {
                if (toNode->type() == RegexGraphicsContainerNode::Type)
                    toNode = qgraphicsitem_cast<RegexGraphicsContainerNode *>(toNode)->getFakeInputNode();
                else
                    toNode = qobject_cast<GraphScene *>(overrideSource->scene())->getSceneOutput();
            }
            RegexNodeConnection* createdConnection = RegexNodeConnection::createConnection(overrideSource, toNode);
            createdConnection->setFake(fakeEdge);
            QDomElement payload = edgeNode.firstChildElement("Payload");
            if (!payload.isNull()) {
                QDomElement payloadNode = payload.firstChildElement("Node");
                RegexGraphicsNode *desNode = deserealizeNode(payloadNode, lookup, NULL, false);
                createdConnection->setPayload(desNode);
            }
        }
    }
}

QString XMLGraphSerialization::deserializeGraph(QString xml, GraphScene *graph)
{
    QHash<QString, RegexGraphicsNode *> nodeLookup;
    return deserializeGraph(xml, graph, nodeLookup);
}

QString XMLGraphSerialization::deserializeGraph(QString xml, GraphScene *graph, QHash<QString, RegexGraphicsNode *> &nodeLookup)
{
    QString errorMessage;
    QDomDocument document;

    if (!document.setContent(xml, &errorMessage)) {
        return errorMessage;
    }

    QDomElement root = document.documentElement();
    //QHash<QString, RegexGraphicsNode *> nodeLookup;

    QDomElement nodes = root.firstChildElement("Nodes");
    if (!nodes.isNull()) {
        QDomNodeList topNodes = nodes.childNodes();
        for (size_t i = 0; i < topNodes.length(); ++i) {
            if (topNodes.at(i).isElement()) {
                if (!deserealizeNode(topNodes.at(i), nodeLookup, graph))
                    return QString("ERROR parsing node");
            }
        }

        QDomNodeList outputs = root.elementsByTagName("Output");
        for (size_t i = 0; i < outputs.length(); ++i) {
            QDomNodeList edgeList = outputs.at(i).toElement().elementsByTagName("Edge");
            deserealizeEdges(edgeList, nodeLookup);
        }

        QDomElement sceneEdges = root.firstChildElement("SceneInputEdges");
        if (!sceneEdges.isNull()) {
            QDomNodeList edgeList = sceneEdges.elementsByTagName("Edge");
            deserealizeEdges(edgeList, nodeLookup, graph->getSceneInput());
        }
    } else {
        return QString("Not a valid graph. No nodes found.");
    }
    return QString();
}



