#include "algorithms.h"
#include "regexabstractnode.h"
#include "regexnodeconnection.h"
#include "regexgraphicsnode.h"


Algorithms::Algorithms()
{
}

QString Algorithms::generateRegex(RegexAbstractNode *start, RegexAbstractNode *end, bool *previousNodeGrouped, bool *ok)
{

    MultipleUseFinder functor;
    QString result;
    bool inOk = true;
    result = generateRegexPrivate(ConnEndPair(NULL, start), ConnEndPair(NULL, end), functor, previousNodeGrouped, &inOk);
    if (ok)
        *ok = inOk;
    return result;
}


class AltFindFunctor
{
public:
    int flow;
    AltFindFunctor(int initialFlow)
    {
        flow = initialFlow;
        startNode = NULL;
    }
    RegexAbstractNode* lastNode;
    bool operator()(RegexAbstractNode* node)
    {
        if (startNode == NULL) {
            startNode = node;
            return false;
        }
        size_t outputCount = 0;
        if (node->isOutputActive())
            outputCount = node->getOutput()->connectionsCount();
        if (node->isInputActive())
            flow -= node->getInput()->connectionsCount();
        if (flow <= 0) {
            lastNode = node;
            return true;
        }
        if (node->isOutputActive()) {
            if (outputCount == 0)
                flow += node->getInput()->connectionsCount();
            flow += outputCount;
        }
        return false;
    }
private:
    RegexAbstractNode *startNode;
};

RegexAbstractNode * Algorithms::findAltEnd(RegexAbstractNode * start)
{
    int flow = 0;
    if (start->isOutputActive())
        flow = start->getOutput()->connectionsCount();
    AltFindFunctor findAlt(flow);
    if (depthFirstSearch(start, findAlt)) {
        return findAlt.lastNode;
    } else {
        return NULL;
    }
}
