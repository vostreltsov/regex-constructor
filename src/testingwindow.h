#ifndef TESTINGWINDOW_H
#define TESTINGWINDOW_H

#include <QDialog>
#include <QRegExp>
#include <QTextBlock>

namespace Ui {
class TestingWindow;
}

class TestingWindow : public QDialog
{
    Q_OBJECT

public:
    explicit TestingWindow(QWidget *parent = 0);
    ~TestingWindow();
    Ui::TestingWindow * getUi();

private:
    Ui::TestingWindow *ui;

private slots:
    void btnTest_clicked(bool checked = false);
};

#endif // TESTINGWINDOW_H
