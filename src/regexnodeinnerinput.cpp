#include "regexnodeinnerinput.h"
#include "regexgraphicsnode.h"

RegexNodeInnerInput::RegexNodeInnerInput(RegexAbstractNode *parent) :
    RegexNodeInput(parent)
{
    adjustPos();
}


int RegexNodeInnerInput::type() const
{
    return Type;
}

void RegexNodeInnerInput::adjustPos()
{
    QPointF inPos(parentNode->boundingRect().width() - 10, parentNode->boundingRect().center().y()) ;
    inPos -= boundingRect().center() - QPointF(0, 3);
    setPos(inPos);
    //RegexGraphicsNodeIOInterface::adjustPos();
}

