#ifndef GRAPHSCENE_H
#define GRAPHSCENE_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include "regexsceneinputnode.h"
#include "regexsceneoutputnode.h"
#include "regexnodeconnection.h"
#include "regexgraphicsnodefactory.h"
#include "graphrubberband.h"
#include "graphdragcontainer.h"
#include <QUndoStack>

class GraphScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GraphScene(QObject *parent = 0);
    virtual ~GraphScene();

    /** @brief Function generates regex from 'start' node.
      * @warning Do not call with any auto connections in graph. */
    void generateRegex();
    /** @brief Creates auto-connections from unconnected outputs. */
    void makeAutoConnections(RegexAbstractNode* start = NULL);
    /** @brief Removes all auto-connections from scene. */
    void removeAutoConnections();
    /** @brief Adds regex node to scene and performs all neccessary actions. */
    void addRegexNode(RegexGraphicsNode *newNode, QPointF scenePos);
    /** @brief Adds node to list of scene regex nodes. */
    void addNodeToContainer(RegexGraphicsNode *node);
    /** @brief Returns generated regex. */
    QString getRegex();
    RegexSceneInputNode *getSceneInput() const;
    RegexSceneOutputNode *getSceneOutput() const;
    QList<RegexAbstractNode *> getRegexNodes();
    template <typename SomeGraphicsItem>
    static QList<SomeGraphicsItem *> findBottomLevel(QList<SomeGraphicsItem *> items);

    /** @brief Deletes all Regex nodes but start and end. */
    void clearNodes();
    /** @brief Renders scene on image. */
    QPixmap getImage(const QRectF &renderRect = QRectF());

    void selectAll();
    void copySelection();
    void paste(QPointF origin);
    void clearUndoStack();
    void createUndoAction();
    QUndoStack *getUndoStack();



protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

    void dropEvent(QGraphicsSceneDragDropEvent *event);
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event);

    bool isAcceptedDrop(QGraphicsSceneDragDropEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

    QString copyInternal();
    GraphDragContainer * pasteInternal(QString str);


signals:
    void hidePropertyEditor();
    void regexChanged(QString, bool);
    void sceneChanged();

public slots:
    void recalculateSceneRect();
    void removeRegexNodeFromContainer(QObject *node);
    void removeSelected();

protected:
    QList<RegexAbstractNode *> regexNodeContainer;
    RegexSceneInputNode *sceneInput;
    RegexSceneOutputNode *sceneOutput;
    QString generatedRegex;
    GraphRubberBand *rubberBand;

    bool dragMultiple;
    GraphDragContainer *dragContainer;
    QClipboard *clipboard;

    QUndoStack undoStack;
};

#endif // GRAPHSCENE_H
