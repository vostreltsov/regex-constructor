#ifndef REGEXGRAPHICSNODEIOINTERFACE_H
#define REGEXGRAPHICSNODEIOINTERFACE_H

#include <QObject>
#include "regexnodeconnection.h"
class RegexAbstractNode;

typedef QPair<RegexNodeConnection*, RegexAbstractNode*> ConnEndPair;

/** @brief Contains common interface fucntions for node inputs/outputs. */

class RegexNodeIOInterface : public QGraphicsObject
{
    Q_OBJECT

protected:

    /** @var Connections from this connection point. */
    QList<RegexNodeConnection *> connections;

    /** @var Parent node, that owns this connection point */
    RegexAbstractNode* parentNode;

    QRectF ioBoundingRect;

public:
    explicit RegexNodeIOInterface(RegexAbstractNode *parent);
    virtual ~RegexNodeIOInterface();

    /** @brief Get all nodes that are directly connected to this connection point.*/
    virtual QList<RegexAbstractNode* > getAllIncidentNodes() = 0;

    virtual QList<RegexNodeConnection *> getConnections();

    virtual QList<ConnEndPair> getIncidentConnectionPairs();

    /** @brief Return parent node. */
    virtual RegexAbstractNode* getParentNode() const;

    virtual size_t connectionsCount();

    void prepareForRemove();

    virtual QRectF boundingRect() const;

signals:

public slots:
    /**
     * @brief Adds a new connection, checks for duplicates.
     * @return true if conenction successfully added.
    */
    virtual bool addConnection(RegexNodeConnection* con);

    /** @brief Removes existing connection from connection point. */
    virtual void removeConnection(RegexNodeConnection* con);

    /** @brief Removes all connections from connection point. */
    virtual void clear();

    /** @brief Positions connections point according to parent item position */
    virtual void adjustPos();

};

#endif // REGEXGRAPHICSNODEIOINTERFACE_H
