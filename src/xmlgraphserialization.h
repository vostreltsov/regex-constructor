#ifndef XMLGRAPHSERIALIZATION_H
#define XMLGRAPHSERIALIZATION_H
#include <QObject>
#include <QString>
#include "regexgraphicsnode.h"
#include "regexgraphicsoperands.h"
#include "regexgraphicsoperators.h"
#include "graphscene.h"

#include <QHash>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QtXml/QDomNodeList>
#include <QXmlStreamWriter>
#include "regexnodeconnection.h"
#include "regexgraphicsnode.h"
#include "regexgraphicsoperators.h"
#include "regexgraphicsoperands.h"
#include "graphscene.h"

class XMLGraphSerialization : public QObject
{
    Q_OBJECT

public:
    static QString serializeGraph(GraphScene *scene);
    static void serializeNode(RegexAbstractNode *node, QXmlStreamWriter &writer);
    static void serializeEdge(RegexNodeConnection* in, QXmlStreamWriter &writer);
    static QString deserializeGraph(QString xml, GraphScene *graph, QHash<QString, RegexGraphicsNode *> &nodeLookup);
    static QString deserializeGraph(QString xml, GraphScene *graph);
    static RegexGraphicsNode* deserealizeNode(QDomNode domNode, QHash<QString, RegexGraphicsNode *> &lookup, GraphScene *scene, bool addToLookup = true);
    static void deserealizeEdges(QDomNodeList edgeList, QHash<QString, RegexGraphicsNode *> &lookup, RegexAbstractNode *overrideSource = NULL);
    static void serializeNodeList(const QList<RegexAbstractNode *> &graph, QXmlStreamWriter &writer);
    static void startSerialization(QXmlStreamWriter &writer);
    static void endSerialization(QXmlStreamWriter &writer);
protected:
    static void addToScene(RegexGraphicsNode *node, GraphScene *graph);
    XMLGraphSerialization(QObject *parent = NULL);

};

#endif // XMLGRAPHSERIALIZATION_H
