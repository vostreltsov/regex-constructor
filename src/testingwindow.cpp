#include "testingwindow.h"
#include "ui_testingwindow.h"

TestingWindow::TestingWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TestingWindow)
{
    ui->setupUi(this);
    connect(ui->btnTest, SIGNAL(clicked(bool)), this, SLOT(btnTest_clicked(bool)));
}

TestingWindow::~TestingWindow()
{
    delete ui;
}

Ui::TestingWindow * TestingWindow::getUi() {
    return ui;
}

void TestingWindow::btnTest_clicked(bool checked) {
    QRegExp regex(ui->lineEditTestingRegex->text(), Qt::CaseSensitive, QRegExp::RegExp2);
    QTextDocument * doc = ui->textEditStrings->document();

    ui->textEditMatches->clear();
    for (int i = 0; i < doc->lineCount(); i++) {
        QString string = doc->findBlockByLineNumber(i).text();

        if (regex.indexIn(string) >= 0) {
            QStringList matches = regex.capturedTexts();
            QString tmp;
            for (int i = 0; i < matches.count(); i++) {
                QString curMatch = matches[i];
                if (curMatch.length() > 0) {
                    tmp += QString::number(i) + ": " + curMatch + ", ";
                }
            }
            if (tmp.endsWith(", ")) {
                tmp = tmp.left(tmp.length() - 2);
            }
            if (tmp.length() == 0) {
                tmp = "empty match";
            }
            ui->textEditMatches->append(tmp);

        } else {
            ui->textEditMatches->append("not matched");
        }
    }
}
