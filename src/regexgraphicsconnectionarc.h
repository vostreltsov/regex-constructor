#ifndef REGEXGRAPHICSCONNECTIONARC_H
#define REGEXGRAPHICSCONNECTIONARC_H

#include <QGraphicsPathItem>
#include <QMenu>

class RegexNodeConnection;

/** @brief Custom PathItem class that handles mouse and keyboard events. */
class RegexGraphicsConnectionArc : public QGraphicsPathItem
{

public:
    RegexGraphicsConnectionArc(RegexNodeConnection* connection, QGraphicsItem *parent = 0, QGraphicsScene* scene = 0);
    enum {Type = UserType + 4};
    int type() const;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    RegexNodeConnection *getOwner();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget); // redefined to remove ugly selection rectangle
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    QPainterPath shape() const; // nicer collision detection

    /** @brief Returns true if reaction to input events is allowed. */
    bool canReactToEvents() const;

    /** @var Connection that ownes this item. */
    RegexNodeConnection* owner;


};

#endif // REGEXGRAPHICSCONNECTIONARC_H
