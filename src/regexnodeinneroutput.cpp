#include "regexnodeinneroutput.h"
#include "regexgraphicsnode.h"
#include "regexnodeinnerinput.h"

RegexNodeInnerOutput::RegexNodeInnerOutput(RegexAbstractNode *parent) :
    RegexNodeOutput(parent)
{
    setZValue(2);
}


int RegexNodeInnerOutput::type() const
{
    return Type;
}

void RegexNodeInnerOutput::adjustPos()
{
    QPointF outPos(boundingRect().width() * 1.4, parentNode->boundingRect().center().y() + boundingRect().height() / 2);
    setPos(outPos);
    //RegexGraphicsNodeIOInterface::adjustPos();
}

bool RegexNodeInnerOutput::isAcceptedItem(QGraphicsItem* item) const
{
    int itemType = item->type();
    switch(itemType) {
    case RegexNodeInput::Type:
        if (item->parentItem()->parentItem() == parentNode)
            return true;
        else
            return false;
    default:
        return false;
    }
}
