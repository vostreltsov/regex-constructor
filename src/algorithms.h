#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <QList>
#include "regexnodeconnection.h"
#include "regexabstractnode.h"
#include "regexnodeinput.h"
#include "regexnodeoutput.h"


/**
 * @brief The Algorithms class contains static methods - graph algorithms.
 */
class Algorithms
{
public:

    /**
     * @brief Depth-first search.
     */
    template <typename Functor>
    static bool depthFirstSearch(RegexAbstractNode* start, Functor& func = EmptyFunctor())
    {
        if (func(start))
            return true;
        /*if (end == start)
            return true;*/
        QList<ConnEndPair> list;
        if (start->isOutputActive())
            list = start->getOutput()->getIncidentConnectionPairs();
        foreach (ConnEndPair pair, list)
            if (!pair.first->isFake())
                if (depthFirstSearch(pair.second, func))
                    return true;
        return false;
    }

    /**
     * @brief Breadth-first search.
     */
    template <typename Functor>
    static bool breadthFirstSearch(RegexAbstractNode* start, Functor& func = EmptyFunctor())
    {
        QList<ConnEndPair> current;
        current.append(ConnEndPair(NULL, start));

        while (!current.isEmpty()) {

            QList<ConnEndPair> next;

            while (!current.isEmpty()) {
                ConnEndPair cur = current.takeLast();
                if (func(cur.second))
                    return true;

                QList<ConnEndPair> output;
                if (cur.second->isOutputActive())
                    output = cur.second->getOutput()->getIncidentConnectionPairs();
                foreach (ConnEndPair nextPair, output) {
                    if (!next.contains(nextPair) && (nextPair.first == NULL || !nextPair.first->isFake()))
                        next.append(nextPair);
                }
            }
            current = next;
        }
        return false;
    }

    /**
     * @brief Generates the resulting regex.
     */
    static QString generateRegex(RegexAbstractNode* start, RegexAbstractNode* end = NULL, bool *previousNodeGrouped = NULL, bool *ok = NULL);

    /**
     * @brief Finds the end node of an alternative.
     */
    static RegexAbstractNode * findAltEnd(RegexAbstractNode * start);

    /** @brief Search will go until the end of graph */
    class EmptyFunctor
    {
    public:
        bool operator()(RegexAbstractNode* node)
        {
            return false;
        }
    };

    /** @brief Makes search to exit when certain node is encountered.*/
    class NodeFinderFunctor
    {
    public:
        NodeFinderFunctor(RegexAbstractNode* end)
        {
            endNode = end;
        }

        RegexAbstractNode* endNode;
        bool operator()(RegexAbstractNode* node)
        {
            return endNode == node;
        }
    };

    /** @brief Makes search to exit when certain node is encountered more than once. */
    class CycleFinderFunctor
    {
    public:
        bool first;
        CycleFinderFunctor(RegexAbstractNode* end)
        {
            endNode = end;
            first = true;
        }

        RegexAbstractNode* endNode;
        bool operator()(RegexAbstractNode* node)
        {
            if (first && endNode == node) {
                first = false;
                return false;
            }
            return endNode == node;
        }
    };

    class MultipleUseFinder
    {
    public:
        MultipleUseFinder()
        {
        }
        bool operator()(RegexAbstractNode* node)
        {
            if (nodes.contains(node))
                return true;
            nodes.append(node);
            return false;
        }
    private:
        QList<RegexAbstractNode *> nodes;
    };
private:
    template <typename Functor>
    static QString generateRegexPrivate(ConnEndPair start, ConnEndPair end, Functor& functor, bool *previousNodeGrouped, bool *ok)
    {
        QString tmp;
        QString result;
        int numberOfConcatenations = 0; // How many times result += ... was called.
        do {
            if (!*ok || functor(start.second)) {
                *ok = false;
                if (start.first)
                    start.first->setValid(false);
                *previousNodeGrouped &= (numberOfConcatenations <= 1);
                return result;
            }
            if (start.first) {
                start.first->setValid(true);
            }

            tmp = start.second->generateRegex(previousNodeGrouped);
            if (!tmp.isEmpty()) {
                result += tmp;
                numberOfConcatenations++;
            }

            QList<ConnEndPair> output;
            if (start.second->isOutputActive())
                output = start.second->getOutput()->getIncidentConnectionPairs();

            // End is reached.
            if (output.empty()) {
                *previousNodeGrouped &= (numberOfConcatenations <= 1);
                return result;
            }

            if (output.size() == 1) {
                // Concatenation.
                start = output.last();
            } else {
                // Alternative.
                RegexAbstractNode *altEnd = findAltEnd(start.second);
                if (output.first().first && output.first().first->getPayload()) {
                    tmp = output.first().first->generatePayloadRegex(previousNodeGrouped);
                    if (!tmp.isEmpty()) {
                        result += tmp;
                        numberOfConcatenations++;
                    }
                }
                tmp = generateRegexPrivate(output.first(), ConnEndPair(NULL, altEnd), functor, previousNodeGrouped, ok);
                if (!tmp.isEmpty()) {
                    result += tmp;
                    numberOfConcatenations++;
                }
                for (QList<ConnEndPair>::const_iterator iter = ++output.constBegin(); iter != output.constEnd(); ++iter) {
                    result += QString("|");
                    if ((*iter).first && (*iter).first->getPayload()) {
                        tmp = (*iter).first->generatePayloadRegex(previousNodeGrouped);
                        //if (!tmp.isEmpty()) {
                            result += tmp;
                            numberOfConcatenations++;
                        //}
                    }
                    tmp = generateRegexPrivate(*iter, ConnEndPair(NULL, altEnd), functor, previousNodeGrouped, ok);
                    //if (!tmp.isEmpty()) {
                        result += tmp;
                        numberOfConcatenations++;
                    //}
                }
                start.second = altEnd;
                if (altEnd) {
                    result = QString("(?:") + result + ")";
                }
            }
            if (start.first) {
                start.first->setValid(true);
                if (start.first->getPayload()) {
                    tmp = start.first->generatePayloadRegex(previousNodeGrouped);
                    if (!tmp.isEmpty()) {
                        result += tmp;
                        numberOfConcatenations++;
                    }

                }
                if (start.first->isFake()) {
                    *previousNodeGrouped &= (numberOfConcatenations <= 1);
                    return result;
                }
            }
        } while(start.second != NULL && start.second != end.second);
        *previousNodeGrouped &= (numberOfConcatenations <= 1);
        return result;
    }

    Algorithms();
};

#endif // ALGORITHMS_H
