#ifndef REGEXGRAPHICSNODEINNEROUTPUT_H
#define REGEXGRAPHICSNODEINNEROUTPUT_H
#include "regexnodeoutput.h"

/** @brief Output that may be used inside the node. */
class RegexNodeInnerOutput : public RegexNodeOutput
{
    Q_OBJECT
public:
    explicit RegexNodeInnerOutput(RegexAbstractNode *parent);
    enum { Type = UserType + 5 };
    int type() const;
signals:

public slots:
    void adjustPos();
protected:
    virtual bool isAcceptedItem(QGraphicsItem* item) const;
};

#endif // REGEXGRAPHICSNODEINNEROUTPUT_H
