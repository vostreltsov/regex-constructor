#ifndef REGEXGRAPHICSNODEINNERINPUT_H
#define REGEXGRAPHICSNODEINNERINPUT_H
#include "regexnodeinput.h"

/** @brief Input that may be used inside the node. */
class RegexNodeInnerInput : public RegexNodeInput
{
    Q_OBJECT

public:
    explicit RegexNodeInnerInput(RegexAbstractNode *parent);
    enum {Type = UserType + 6};
    int type() const;
public slots:
    void adjustPos();
};

#endif // REGEXGRAPHICSNODEINNERINPUT_H
