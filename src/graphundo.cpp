#include "graphundo.h"
#include "xmlgraphserialization.h"

GraphUndo::GraphUndo(GraphScene *scene)
{
    qDebug("New undo action");
    graph = scene;
    serializeIn(oldState);
    firstRedoCall = true;
}

void GraphUndo::undo()
{
    if (newState.size() == 0)
        serializeIn(newState);
    deserializeFrom(oldState);
}

void GraphUndo::redo()
{
    if (firstRedoCall)
        firstRedoCall = false;
    else
        deserializeFrom(newState);
}

void GraphUndo::serializeIn(QByteArray &arr)
{
    QString serialized = XMLGraphSerialization::serializeGraph(graph);
    arr = qCompress(serialized.toUtf8());
}

void GraphUndo::deserializeFrom(QByteArray &arr)
{
    graph->clearNodes();
    XMLGraphSerialization::deserializeGraph(QString::fromUtf8(qUncompress(arr)), graph);
}
