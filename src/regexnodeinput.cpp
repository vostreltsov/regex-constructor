#include "regexnodeinput.h"
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QTextStream>
#include <QPainter>
#include "regexabstractnode.h"
#include "regexnodeconnection.h"
#include <QList>
#include "graphscene.h"
#include <QPropertyAnimation>




RegexNodeInput::RegexNodeInput(RegexAbstractNode *parent) :
    RegexNodeIOInterface(parent),
    acceptsManual(true),
    dropEntered(false)
{
    adjustPos();
    setZValue(10);
}


void RegexNodeInput::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    RegexNodeIOInterface::adjustPos();
    QBrush brush;
    brush.setColor(QColor(0, 0, 0));
    brush.setStyle(Qt::SolidPattern);
    painter->setBrush(brush);
    painter->drawEllipse(boundingRect());
    brush.setColor(QColor(255, 128, 0));
    painter->setBrush(brush);
    painter->drawEllipse(boundingRect().adjusted(1, 1, -1, -1));
}

void RegexNodeInput::adjustPos()
{
    qreal xPos = boundingRect().width() / 2;
    qreal yPos = parentNode->boundingRect().height() / 2 + boundingRect().height() / 2;
    setPos(xPos, yPos);
    //RegexGraphicsNodeIOInterface::adjustPos();
}

int RegexNodeInput::type() const
{
    return Type;
}

QList<RegexAbstractNode *> RegexNodeInput::getAllIncidentNodes()
{
    QList<RegexAbstractNode* > inc;
    foreach (RegexNodeConnection *node, connections)
        inc.append(node->getSource());
    return inc;
}

bool RegexNodeInput::addConnection(RegexNodeConnection* con)
{
    con->setDestination(this);
    return RegexNodeIOInterface::addConnection(con);
}

QPainterPath RegexNodeInput::shape() const
{
    if (acceptsManual)
        return QGraphicsObject::shape();
    else
        return QPainterPath();
}

void RegexNodeInput::acceptManualConnections(bool accept)
{
    acceptsManual = accept;
}

void RegexNodeInput::animateAcceptDropEnter()
{
    if (!dropEntered) {
        dropEntered = true;
        setTransformOriginPoint(boundingRect().center());
        QPropertyAnimation *anim = new QPropertyAnimation(this);
        anim->setDuration(100);
        anim->setTargetObject(this);
        anim->setPropertyName("scale");
        anim->setStartValue(1.0);
        anim->setEndValue(2);
        connect(anim, SIGNAL(finished()), anim, SLOT(deleteLater()));
        anim->start();
    }
}

void RegexNodeInput::animateAcceptDropLeave()
{
    if (dropEntered) {
        setTransformOriginPoint(boundingRect().center());
        QPropertyAnimation *anim = new QPropertyAnimation(this);
        anim->setDuration(100);
        anim->setTargetObject(this);
        anim->setPropertyName("scale");
        anim->setStartValue(2);
        anim->setEndValue(1);
        connect(anim, SIGNAL(finished()), anim, SLOT(deleteLater()));
        anim->start();
        dropEntered = false;
    }
}
