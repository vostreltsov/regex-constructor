#include "regexsceneinputnode.h"
#include <QPainter>
#include "globalconstants.h"
#include "graphscene.h"
#include "algorithms.h"
#include <QGraphicsRotation>

RegexSceneInputNode::RegexSceneInputNode(QGraphicsItem *parent) :
    RegexAbstractNode(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable, false);
    bounding = QRectF(QPointF(0, 0), QPointF(40, 200));
    activateOutput();
}


QVariant RegexSceneInputNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (QGraphicsItem::ItemSceneChange == change) {
        if (value.value<QGraphicsScene *>() != 0) {
            QGraphicsScene *newScene = value.value<QGraphicsScene *>();
            setPos(0, newScene->height() / 2 - boundingRect().height() / 2);
        }
        return value;
    }
    return QGraphicsObject::itemChange(change, value);
}


void RegexSceneInputNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    static const QPointF poly[5] = {
        QPointF(0, 0),
        QPointF(0, 200),
        QPointF(20, 200),
        QPointF(40, 100),
        QPointF(20, 0)
    };


    painter->setBrush(GlobalConst::fillColor);
    painter->setPen(GlobalConst::shadowColor);
    painter->drawPolygon(poly, 5);

    painter->setBrush(GlobalConst::separatorColor);
    painter->setPen(GlobalConst::separatorColor);
    painter->setFont(GlobalConst::labelFont);
    painter->drawText(boundingRect().translated(-5, 0), Qt::AlignCenter, "S\nT\nA\nR\nT");

}

QRectF RegexSceneInputNode::boundingRect() const
{
    return bounding;
}

QString RegexSceneInputNode::generateRegex()
{
    return QString("");
}

int RegexSceneInputNode::type() const
{
    return Type;
}
