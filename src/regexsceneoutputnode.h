#ifndef REGEXSCENEOUTPUT_H
#define REGEXSCENEOUTPUT_H

#include "regexabstractnode.h"

class RegexSceneOutputNode : public RegexAbstractNode
{
    Q_OBJECT
public:
    explicit RegexSceneOutputNode(QGraphicsItem *parent = 0);

    QRectF boundingRect() const;

    QString generateRegex();

    enum {Type = UserType + 21};
    virtual int type() const;

signals:

public slots:

protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF bounding;

};

#endif // REGEXSCENEOUTPUT_H
