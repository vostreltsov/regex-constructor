#include "graphdragcontainer.h"
#include "regexgraphicscontainernode.h"
#include <QPainter>

GraphDragContainer::GraphDragContainer(QGraphicsScene *scene, QGraphicsItem *parent) :
    RegexAbstractNode(parent),
    borderPen(GlobalConst::multipleDragColor)
{
    scene->addItem(this);
    setFlag(QGraphicsItem::ItemIsMovable);
    borderPen.setWidthF(2.5);
}

GraphDragContainer::~GraphDragContainer()
{

}

bool GraphDragContainer::isEmpty()
{
    return contained.isEmpty();
}

QList<RegexAbstractNode *> GraphDragContainer::getPureNodes(QList<QGraphicsItem *> nodes)
{
    QList<RegexAbstractNode *> pureNodes;
    foreach (QGraphicsItem *node, nodes) {
        if (node->type() != RegexGraphicsConnectionArc::Type)
            pureNodes.append(static_cast<RegexAbstractNode *>(node));
    }
    return pureNodes;
}

void GraphDragContainer::acquire(QList<QGraphicsItem *> nodes)
{
    //prepareGeometryChange();
    if (nodes.isEmpty())
        return;
    QRectF newBRect;
    QList<RegexAbstractNode *> pureNodes;
    foreach (QGraphicsItem *node, nodes) {
        if (node->type() != RegexGraphicsConnectionArc::Type) {
            newBRect |= node->sceneBoundingRect();
            pureNodes.append(static_cast<RegexAbstractNode *>(node));
        }
    }
    prepareGeometryChange();
    newBRect.adjust(-4, -4, 4, 4);
    bRect = newBRect;
    bRect.moveTo(0, 0);
    setPos(newBRect.topLeft());

    RegexAbstractNode *bottom = RegexAbstractNode::findBottomNode(pureNodes);
    RegexGraphicsContainerNode *parent = qgraphicsitem_cast<RegexGraphicsContainerNode *>(bottom->parentItem());
    foreach (RegexAbstractNode *node, pureNodes) {
        if (parent == node->parentItem()) {
            if (parent)
                parent->forceRemoveFromContainer(node);
            node->setParentItem(this);
            node->setPos(mapFromItem(parent, node->pos()));
            contained.append(node);
        }
    }
    previous = parent;

}

bool GraphDragContainer::changeContainer()
{
    bool res = RegexAbstractNode::changeContainer();
    shiftCollidingItems();
    release();
    return res;
}

void GraphDragContainer::release()
{
    RegexGraphicsContainerNode *parent = qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentItem());
    foreach (RegexAbstractNode *node, contained) {
        QPointF pos = node->pos();
        if (parent)
            parent->forceAddToContainer(node);
        else
            node->setParentItem(NULL);
        node->setPos(mapToItem(parent, pos));
        node->setSelected(true);
    }
    if (previous != parent) {
        foreach (RegexAbstractNode *node, contained) {
            foreach (RegexNodeConnection *con, node->getInput()->getConnections()) {
                if (!contained.contains(con->getSource()))
                    delete con;
            }
            foreach (RegexNodeConnection *con, node->getOutput()->getConnections()) {
                if (!contained.contains(con->getDestination()))
                    delete con;
            }
        }
    }
    if (parent)
        parent->fitFrame();
    if (previous && previous != parent)
        previous->fitFrame();
    contained.clear();

    deleteLater();
}

QRectF GraphDragContainer::boundingRect() const
{
    return bRect;
}

void GraphDragContainer::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::NoBrush);
    painter->setPen(borderPen);
    painter->drawRoundedRect(bRect.adjusted(2, 2, -2, -2), 2, 2);
}


void GraphDragContainer::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    changeContainer();
    event->ignore();
    QGraphicsObject::mouseReleaseEvent(event);
}

QList<RegexAbstractNode *> GraphDragContainer::getContained()
{
    return contained;
}
