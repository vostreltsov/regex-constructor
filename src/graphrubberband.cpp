#include "graphrubberband.h"
#include <QPainter>
#include <QPen>
#include "globalconstants.h"

GraphRubberBand::GraphRubberBand(QPointF startPos, QGraphicsScene *scene) :
    QGraphicsRectItem(startPos.x(), startPos.y(), 0, 0, NULL, scene),
    QObject(scene),
    start(startPos),
    lastEnd(startPos)
{
    pen.setColor(GlobalConst::shadowColor);
    pen.setStyle(Qt::DashDotDotLine);
    brush.setColor(GlobalConst::shadowColor);
    brush.setStyle(Qt::NoBrush);
    setPen(pen);
    setBrush(brush);
    canChange = true;
    QTimer *timer = new QTimer();
    timer->setInterval(20);
    connect(timer, SIGNAL(timeout()), this, SLOT(setCanChange()));
    connect(this, SIGNAL(destroyed()), timer, SLOT(deleteLater()));
    timer->start();
}

bool GraphRubberBand::setEndpoint(QPointF end)
{
    if (canChange) {
        lastEnd = end;
        QPointF size = end - start;
        qreal width = size.x();
        qreal height = size.y();
        QPointF newStart(start);
        if (width < 0)
            newStart.setX(start.x() + width);
        if (height < 0)
            newStart.setY(start.y() + height);
        setRect(newStart.x(), newStart.y(), abs(width), abs(height));
        canChange = false;
        return true;
    }
    return false;
}

void GraphRubberBand::setCanChange()
{
    canChange = true;
}

