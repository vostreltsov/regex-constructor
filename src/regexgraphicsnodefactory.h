#ifndef REGEXGRAPHICSNODEFACTORY_H
#define REGEXGRAPHICSNODEFACTORY_H

#include <QMainWindow>
#include <QList>
#include "regexgraphicsnode.h"
#include "regexgraphicsoperators.h"
#include "regexgraphicsoperands.h"

/**
 * Namespace containing a fancy factory of regex nodes.
 */
class RegexGraphicsNodeFactory
{
protected:

    /** @var Pointer to a main window, dirty hack. */
    static QMainWindow *mainWindow;

public:

    /**
     * @brief Sets the main window pointer.
     */
    static void setMainWindow(QMainWindow * mW);

    /**
     * @brief Returns main window. Can be used with connect etc...
     */
    static QMainWindow * getMainWindow();

    /**
     * @brief Returns available node types (RegexNodeType enum items).
     */
    static QList<int> availableNodeTypes();

    /**
     * @brief Creates a node of the given type and returns a pointer to it.
     */
    static RegexGraphicsNode * get(int type, QGraphicsItem * parent = NULL, qreal x = 0, qreal y = 0);

};


#endif // REGEXGRAPHICSNODEFACTORY_H
