#ifndef REGEXGRAPHICSCONTAINERNODE_H
#define REGEXGRAPHICSCONTAINERNODE_H
#include "regexgraphicsnode.h"
#include "regexnodeinnerinput.h"
#include "regexnodeinneroutput.h"

class QPropertyAnimation;

/** @brief Node with an inner structure. */
class RegexGraphicsContainerNode : public RegexGraphicsNode
{
    Q_OBJECT
public:
    explicit RegexGraphicsContainerNode(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);
    virtual ~RegexGraphicsContainerNode();
    bool addToContainer(RegexAbstractNode* node);
    bool removeFromContainer(RegexAbstractNode *node);
    virtual QPainterPath shape() const;
    virtual QString generateRegex(bool *previousNodeGrouped);
    RegexAbstractNode *getFakeInputNode();
    RegexNodeInnerInput *getFakeInnerInput();
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    QList<RegexAbstractNode *> getContainedNodes();


    enum { Type = UserType + 10};
    virtual int type() const;

    bool changeBoundingRect(QRectF rect);
    void forceAddToContainer(RegexAbstractNode* node);
    void forceRemoveFromContainer(RegexAbstractNode* node);

signals:

public slots:
    /** @brief Resizes and repositions node to cover all inner nodes. */
    void fitFrame();

    void animateHoverEntered();
    void animateHoverLeaved();
protected:
    RegexNodeInnerInput* innerInput;
    RegexNodeInnerOutput* innerOutput;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual void drawMainRect(QPainter *painter, QBrush brush, QPen pen);
    virtual QPainterPath opaqueArea() const;
    virtual QRectF drawLabelArea(QPainter *painter, QBrush brush, QPen pen);

    QPainterPath opaqueRect;
    class FakeInputNode;
    class FakeInnerInput;
    FakeInputNode* fakeInput;
    FakeInnerInput* fakeInnerInput;
    QList<RegexAbstractNode *> containedNodes;
    bool containerHoverEntered;
    QPropertyAnimation *hoverOverAnimation;
};


class RegexGraphicsContainerNode::FakeInputNode : public RegexAbstractNode
{
    Q_OBJECT
public:
    explicit FakeInputNode(RegexNodeInput *in, RegexNodeOutput * out) :
        RegexAbstractNode(NULL)
    {
        input = in;
        output = out;
    }
    virtual ~FakeInputNode()
    {
        input = NULL;
        output = NULL;
    }

    QRectF boundingRect() const
    {
        return QRectF();
    }

    QString generateRegex(bool *previousNodeGrouped)
    {
        return "";
    }
protected:

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
    }

};

class RegexGraphicsContainerNode::FakeInnerInput
        : public RegexNodeInnerInput
{

    Q_OBJECT

public:
    FakeInnerInput(RegexAbstractNode *parent) :
        RegexNodeInnerInput(parent)
    {
    }
    /** @brief Get all nodes that are directly connected to this connection point.*/
    virtual QList<RegexAbstractNode* > getAllIncidentNodes()
    {
        return QList<RegexAbstractNode* >();
    }

    virtual QList<RegexNodeConnection *> getConnections()
    {
        return QList<RegexNodeConnection *>();
    }

    virtual QList<ConnEndPair> getIncidentConnectionPairs()
    {
        return QList<ConnEndPair>();
    }

    virtual size_t connectionsCount()
    {
        return 0;
    }

    virtual void adjustPos()
    {
        qreal xPos = parentNode->boundingRect().width() + boundingRect().width() / 2;
        qreal yPos = parentNode->boundingRect().height() / 2 + boundingRect().height() / 2;
        setPos(xPos, yPos);
        RegexNodeIOInterface::adjustPos();
    }

signals:

public slots:

    virtual QPainterPath shape() const
    {
        return QPainterPath();
    }

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
    {
    }
};



#endif // REGEXGRAPHICSCONTAINERNODE_H
