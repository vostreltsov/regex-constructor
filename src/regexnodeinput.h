#ifndef REGEXGRAPHICSNODEINPUT_H
#define REGEXGRAPHICSNODEINPUT_H

#include <QGraphicsEllipseItem>
#include <QObject>
#include <QList>
#include "regexnodeiointerface.h"


/** @brief Input connection point. */
class RegexNodeInput : public RegexNodeIOInterface
{
    Q_OBJECT

public:
    explicit RegexNodeInput(RegexAbstractNode *parent);
    QList<RegexAbstractNode* > getAllIncidentNodes();
    enum {Type = UserType + 2};
    virtual int type() const;
    void acceptManualConnections(bool accept);
    QPainterPath shape() const;


public slots:
    virtual void adjustPos();
    virtual bool addConnection(RegexNodeConnection* con);
    void animateAcceptDropEnter();
    void animateAcceptDropLeave();

signals:

protected:
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    bool acceptsManual;
    bool dropEntered;
};

#endif // REGEXGRAPHICSNODEINPUT_H
