#ifndef REGEXGRAPHICSNODE_H
#define REGEXGRAPHICSNODE_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QList>
#include <QColor>
#include <QString>
#include <QMenu>
#include "regexabstractnode.h"
//#include "regexgraphicsnodeiointerface.h"

class RegexNodeIOInterface;
class RegexNodeConnection;

/**
 * @brief The general class for graphical regex node representation.
 */
class RegexGraphicsNode : public RegexAbstractNode
{
    Q_OBJECT

public:
    // Types of supported operators and operands.
    enum RegexNodeType
    {
        TYPE_NODE_ABSTRACT,
        TYPE_NODE_FINITE_QUANT,
        TYPE_NODE_INFINITE_QUANT,
        TYPE_NODE_SUBPATT,
        TYPE_LEAF_CHARSET,
        TYPE_LEAF_ASSERT,
        TYPE_LEAF_DOT,
        TYPE_LEAF_STRING,
        TYPE_NODE_GROUP // ADD NEW LEAF TYPES TO END OF ENUM OR YOU WILL BREAK SAVE-FILES
    };
    static const QString NODE_FROM_TOOLBOX_MIME_DATA;
    explicit RegexGraphicsNode(QGraphicsItem *parent = 0, qreal x = 0, qreal y = 0);

    virtual ~RegexGraphicsNode();

    /**
      * @brief Enables or disables drag-n-drop mode, in disabled mode you still can move the object
      * in enabled mode you can drag object from current View to another.
      */
    void enableDrag(bool enable);


    /*---------------------------------------- Getters ----------------------------------------*/

    QString getLabel() const;
    QString getText() const;

    /*----------------------------------- Interface methods -----------------------------------*/

    /**
     * @brief Returns type of this node (see the RegexNodeType enum). Type is set when node is created and can not be modified.
     */
    virtual RegexNodeType nodeType() const;

    enum {Type = UserType + 1};
    virtual int type() const;
    virtual QRectF boundingRect() const;
    virtual QPainterPath shape() const;

    /** @brief Changes bounding rect, preliminarily moving rect to 0, 0. */
    virtual bool changeBoundingRect(QRectF rect);


public slots:

    void emitPropertiesSignal(RegexNodeConnection *conn = NULL);

    /*---------------------------------------- Setters ----------------------------------------*/

    void setLabel(const QString & value);
    virtual void updateText();

    /** @brief Enables item movement by mouse */
    void enableMovement(bool enabled = true);

signals:
    void openPropertyEditor(RegexGraphicsNode* node);
    void openPropertyEditor(RegexNodeConnection *conn);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    /**
      * @brief draws node rectangle and shadow
      */
    virtual void drawMainRect(QPainter *painter, QBrush brush, QPen pen);
    /**
      * @brief draws node label area and label text, can change node size to fit the label.
      * @return rectangle occupied by label
      */
    virtual QRectF drawLabelArea(QPainter *painter, QBrush brush, QPen pen);
    /**
      * @brief draws inner text, changes node size to fit the text.
      * @return rectangle occupied by text
      */
    virtual QRectF drawText(QPainter *painter, QBrush brush, QPen pen, QRectF textArea);

    /** @brief Renders node from scene to a pixmap. */
    QPixmap getNodeImage();

    void constructPopupMenu();


    float roundRadius;

    /** @var Node caption displayed to user. */
    QString label;

    /** @var Node text displayed to user. */
    QString text;

    /** @var Context menu. */
    QMenu popupMenu;

    /** @var Contains bounding rect. */
    QRectF bounding;

};

#endif // REGEXGRAPHICSNODE_H
