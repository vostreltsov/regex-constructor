#ifndef GRAPHIO_H
#define GRAPHIO_H

#include "graphio.h"
#include "regexgraphicsnode.h"
#include "xmlgraphserialization.h"
#include "graphscene.h"

#include <iostream>

#include <QFile>
#include <QString>
#include <QTextStream>
#include <QVector>

class GraphIO
{
public:
    static int loadFromFile(QString filePath, GraphScene *loadedGraph);
    static int saveToFile(QString filePath, GraphScene *storedGraph);
};

#endif // GRAPHIO_H
