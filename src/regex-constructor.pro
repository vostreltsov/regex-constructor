#-------------------------------------------------
#
# Project created by QtCreator 2012-09-05T23:14:57
#
#-------------------------------------------------

QT += core gui
QT += xml
win32:RC_FILE = regex-constructor.rc

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = regex-constructor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    graphscene.cpp \
    regexgraphicsnode.cpp \
    toolboxscenecontainer.cpp \
    regexgraphicsoperators.cpp \
    regexgraphicsoperands.cpp \
    regexgraphicsnodefactory.cpp \
    regexgraphicsconnectionarc.cpp \
    regexgraphicscontainernode.cpp \
    algorithms.cpp \
    regexsceneinputnode.cpp \
    regexabstractnode.cpp \
    regexsceneoutputnode.cpp \
    testingwindow.cpp \
    graphio.cpp \
    xmlgraphserialization.cpp \
    regexnodeconnection.cpp \
    regexnodeiointerface.cpp \
    regexnodeoutput.cpp \
    regexnodeinput.cpp \
    regexnodeinneroutput.cpp \
    regexnodeinnerinput.cpp \
    graphrubberband.cpp \
    graphdragcontainer.cpp \
    graphundo.cpp

HEADERS  += mainwindow.h \
    graphscene.h \
    regexgraphicsnode.h \
    toolboxscenecontainer.h \
    regexgraphicsoperators.h \
    regexgraphicsoperands.h \
    regexgraphicsnodefactory.h \
    regexgraphicsconnectionarc.h \
    globalconstants.h \
    regexgraphicscontainernode.h \
    algorithms.h \
    regexsceneinputnode.h \
    regexabstractnode.h \
    regexsceneoutputnode.h \
    testingwindow.h \
    xmlgraphserialization.h \
    graphio.h \
    xmlgraphserialization.h \
    translation.h \
    regexnodeconnection.h \
    regexnodeinput.h \
    regexnodeiointerface.h \
    regexnodeoutput.h \
    regexnodeinnerinput.h \
    regexnodeinneroutput.h \
    graphrubberband.h \
    graphdragcontainer.h \
    graphundo.h

FORMS    += mainwindow.ui \
    testingwindow.ui

RESOURCES += \
    resources.qrc
