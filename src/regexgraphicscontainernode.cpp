#include "regexgraphicscontainernode.h"
#include "globalconstants.h"
#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QColor>
#include "algorithms.h"
#include <QPair>
#include <QPropertyAnimation>



RegexGraphicsContainerNode::RegexGraphicsContainerNode(QGraphicsItem *parent, qreal x, qreal y) :
    RegexGraphicsNode(parent, x, y),
    innerOutput(new RegexNodeInnerOutput(this)),
    innerInput(NULL/*new RegexGraphicsNodeInnerInput(this)*/),
    fakeInnerInput(new FakeInnerInput(this)),
    containerHoverEntered(false),
    hoverOverAnimation(NULL)
{
    fakeInput = new FakeInputNode(fakeInnerInput, innerOutput);
    setZValue(-1);
}

QVariant RegexGraphicsContainerNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    /*if (QGraphicsItem::ItemPositionHasChanged == change || QGraphicsItem::ItemScaleHasChanged == change) {
        if (innerInput)
            innerInput->adjustPos();
        if (innerOutput)
            innerOutput->adjustPos();
        if (fakeInnerInput)
            fakeInnerInput->adjustPos();
    }*/
    return RegexGraphicsNode::itemChange(change, value);
}

RegexGraphicsContainerNode::~RegexGraphicsContainerNode()
{
    input->clear();
    if (innerOutput)
        delete innerOutput;
    if (innerInput)
        delete innerInput;
    if (fakeInput)
        delete fakeInput;
    if (fakeInnerInput)
        delete fakeInnerInput;
}

bool RegexGraphicsContainerNode::addToContainer(RegexAbstractNode *node)
{
    if (!containedNodes.contains(node)) {
        QPointF oldPos = node->pos();
        if (node->isInputActive())
            node->getInput()->clear();
        if (node->isOutputActive())
            node->getOutput()->clear();
        RegexGraphicsContainerNode *previousParent = NULL;
        if (node->parentItem() && node->parentItem()->type() == RegexGraphicsContainerNode::Type) {
            previousParent = qgraphicsitem_cast<RegexGraphicsContainerNode *>(node->parentItem());
        }
        node->setParentItem(this);
        if (containedNodes.size() == 0 && node->isInputActive()) {
            RegexNodeConnection* firstConnection =
                    new RegexNodeConnection(innerOutput, node->getInput());
            innerOutput->addConnection(firstConnection);
            node->getInput()->addConnection(firstConnection);
            ioStateChanged();
        }
        containedNodes.append(node);
        node->setPos(mapFromItem(previousParent, oldPos));
        if (previousParent)
            previousParent->fitFrame();
        fitFrame();
        return true;
    } else {
        fitFrame();
        return false;
    }
}
void RegexGraphicsContainerNode::forceAddToContainer(RegexAbstractNode* node)
{
    node->setParentItem(this);
    containedNodes.append(node);
}

bool RegexGraphicsContainerNode::removeFromContainer(RegexAbstractNode *node)
{
    if (containedNodes.contains(node)) {
        QPointF oldPos = node->pos();
        if (node->isInputActive())
            node->getInput()->clear();
        if (node->isOutputActive())
            node->getOutput()->clear();
        if (node->type() != RegexAbstractNode::Type) {
            node->setParentItem(NULL);
            node->setPos(this->mapToScene(oldPos));
            fitFrame();
        }
        containedNodes.removeOne(node);
        return true;
    }
    return false;
}

void RegexGraphicsContainerNode::forceRemoveFromContainer(RegexAbstractNode *node)
{
    node->setParentItem(NULL);
    containedNodes.removeOne(node);
}

QList<RegexAbstractNode *> RegexGraphicsContainerNode::getContainedNodes()
{
    return containedNodes;
}


void RegexGraphicsContainerNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    RegexGraphicsNode::paint(painter, option, widget);
}

void RegexGraphicsContainerNode::drawMainRect(QPainter *painter, QBrush brush, QPen pen)
{
    brush.setStyle(Qt::NoBrush);
    brush.setColor(GlobalConst::shadowColor);
    if (isSelected())
        painter->setPen(GlobalConst::activeSelectionColor);
    else
        painter->setPen(GlobalConst::shadowColor);
    painter->setBrush(brush);
    painter->drawRoundedRect(boundingRect(), roundRadius, roundRadius);

    brush.setColor(GlobalConst::fillColor);
    painter->setBrush(brush);
    painter->drawRoundedRect(boundingRect().adjusted(1, 1, -2, -1), roundRadius, roundRadius);
}

void RegexGraphicsContainerNode::fitFrame()
{
    QRectF newRect(sceneBoundingRect().center(), sceneBoundingRect().center());
    foreach (QGraphicsItem* item, childItems()) {
        if (item->type() == RegexGraphicsContainerNode::Type
                || item->type() == RegexGraphicsNode::Type) {
            newRect |= item->sceneBoundingRect();
        }
    }

    typedef QPair<QGraphicsItem*, QPointF> PositionPair;
    QList<PositionPair> oldPositions;
    foreach (QGraphicsItem* node, childItems()) {
        if (node->type() == RegexGraphicsNode::Type || node->type() == RegexGraphicsContainerNode::Type)
            oldPositions.append(qMakePair(node, node->scenePos()));
    }
    newRect.adjust(-50, -40, 50, 40); // some frame
    changeBoundingRect(newRect); // only size
    if (parentItem())
        setPos(parentItem()->mapFromScene(newRect.topLeft())); // pos for parented items
    else
        setPos(newRect.topLeft()); // pos fro scene items

    foreach (PositionPair pair, oldPositions) {
        pair.first->setPos(mapFromScene(pair.second));
    }
    shiftCollidingItems();
    if (parentItem() && parentItem()->type() == RegexGraphicsContainerNode::Type)
        qgraphicsitem_cast<RegexGraphicsContainerNode *>(parentItem())->fitFrame();

}

QPainterPath RegexGraphicsContainerNode::opaqueArea() const
{
    return opaqueRect;
}

QRectF RegexGraphicsContainerNode::drawLabelArea(QPainter *painter, QBrush brush, QPen pen)
{
    QRectF labelRect = RegexGraphicsNode::drawLabelArea(painter, brush, pen);
    QPainterPath path;
    path.addRect(boundingRect() & labelRect);
    opaqueRect = path;
    return labelRect;
}

QPainterPath RegexGraphicsContainerNode::shape() const
{
    return opaqueArea();
}

int RegexGraphicsContainerNode::type() const
{
    return Type;
}

QString RegexGraphicsContainerNode::generateRegex(bool *previousNodeGrouped)
{
    return Algorithms::generateRegex(getFakeInputNode(), this, previousNodeGrouped);
}

RegexAbstractNode *RegexGraphicsContainerNode::getFakeInputNode()
{
    return fakeInput;
}

RegexNodeInnerInput *RegexGraphicsContainerNode::getFakeInnerInput()
{
    return fakeInnerInput;
}

bool RegexGraphicsContainerNode::changeBoundingRect(QRectF rect)
{
    if (RegexGraphicsNode::changeBoundingRect(rect)) {
        if (innerInput)
            innerInput->adjustPos();
        if (innerOutput)
            innerOutput->adjustPos();
        if (fakeInnerInput)
            fakeInnerInput->adjustPos();
        return true;
    }
    return false;
}


void RegexGraphicsContainerNode::animateHoverEntered()
{
    if (!containerHoverEntered && !hoverOverAnimation) {
        containerHoverEntered = true;
        hoverOverAnimation = new QPropertyAnimation(this);
        hoverOverAnimation->setDuration(1000);
        hoverOverAnimation->setTargetObject(this);
        hoverOverAnimation->setPropertyName("opacity");
        hoverOverAnimation->setStartValue(1);
        hoverOverAnimation->setKeyValueAt(0.5, 0.5);
        hoverOverAnimation->setEndValue(1);
        hoverOverAnimation->setLoopCount(-1);
        hoverOverAnimation->start();
    }
}

void RegexGraphicsContainerNode::animateHoverLeaved()
{
    if (containerHoverEntered && hoverOverAnimation) {
        hoverOverAnimation->stop();
        delete hoverOverAnimation;
        hoverOverAnimation = NULL;
        setOpacity(1);
        containerHoverEntered = false;
    }
}
