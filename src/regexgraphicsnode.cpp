#include "regexgraphicsnode.h"
#include <QApplication>
#include <QDrag>
#include <QCursor>
#include <QMimeData>
#include <QPainter>
#include <QPixmap>
#include <QKeySequence>
#include <QGraphicsSceneContextMenuEvent>
#include <QKeyEvent>
#include "regexgraphicscontainernode.h"
#include "globalconstants.h"
#include "graphscene.h"

const QString RegexGraphicsNode::NODE_FROM_TOOLBOX_MIME_DATA = QString("#TBOX#:");

RegexGraphicsNode::RegexGraphicsNode(QGraphicsItem *parent, qreal x, qreal y) :
    RegexAbstractNode(parent), bounding(0, 0, 128, 48)
{
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    setPos(x, y);
    label = QString("");
    text = QString("");
    activateInput();
    activateOutput();
}

RegexGraphicsNode::~RegexGraphicsNode()
{
}

void RegexGraphicsNode::enableDrag(bool enable)
{
    inToolbox = enable;
}


void RegexGraphicsNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (inToolbox) {
        setCursor(Qt::ClosedHandCursor);
    } else {
        RegexAbstractNode::mousePressEvent(event);
    }
}

void RegexGraphicsNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (inToolbox) {
        if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton)).length() < QApplication::startDragDistance()) {
            return;
        }

        QDrag *drag = new QDrag(event->widget());
        QMimeData *mime = new QMimeData;
        QString typeText = NODE_FROM_TOOLBOX_MIME_DATA + QString::number(nodeType());
        mime->setText(typeText);
        drag->setMimeData(mime);
        drag->setPixmap(getNodeImage());
        drag->setHotSpot(QPoint(6, 1));
        drag->exec(Qt::CopyAction);
        setCursor(Qt::OpenHandCursor);
    } else {
        RegexAbstractNode::mouseMoveEvent(event);
    }
}

void RegexGraphicsNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (inToolbox) {
        setCursor(Qt::ArrowCursor);
    } else {
        RegexAbstractNode::mouseReleaseEvent(event);
    }
}





void RegexGraphicsNode::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (inToolbox) {
        event->ignore();
    } else {
        QGraphicsObject::mouseDoubleClickEvent(event);
        emit openPropertyEditor(this);
    }
}

void RegexGraphicsNode::keyPressEvent(QKeyEvent *event)
{
    if (inToolbox) {
        event->ignore();
    } else {
        int key = event->key();
        if (key == Qt::Key_Delete || key == Qt::Key_X)
            removeByUser();
    }
}

void RegexGraphicsNode::keyReleaseEvent(QKeyEvent *event)
{
    event->accept();
    QGraphicsObject::keyReleaseEvent(event);
}

QString RegexGraphicsNode::getLabel() const
{
    return label;
}

QString RegexGraphicsNode::getText() const
{
    return text;
}

void RegexGraphicsNode::setLabel(const QString & value)
{
    if (label != value) {
        label = value;
        ioStateChanged(); // that's not entirely true, but we need to regenerate regex
    }
}

void RegexGraphicsNode::updateText()
{
    ioStateChanged(); // that's not entirely true, but we need to regenerate regex
    update();
}

RegexGraphicsNode::RegexNodeType RegexGraphicsNode::nodeType() const
{
    return TYPE_NODE_ABSTRACT;
}

void RegexGraphicsNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setRenderHint(QPainter::Antialiasing);
    QBrush brush;
    QPen pen;

    roundRadius = 2; // for sanity reasons
    drawMainRect(painter, brush, pen);
    QRectF labelArea = drawLabelArea(painter, brush, pen);
    QRectF textArea = boundingRect().adjusted(0, labelArea.height() + 8, 0, 0);
    drawText(painter, brush, pen, textArea);
}

void RegexGraphicsNode::drawMainRect(QPainter *painter, QBrush brush, QPen pen)
{
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(GlobalConst::shadowColor);
    if (isSelected())
        painter->setPen(GlobalConst::activeSelectionColor);
    else
        painter->setPen(GlobalConst::shadowColor);
    painter->setBrush(brush);
    painter->drawRoundedRect(boundingRect(), roundRadius, roundRadius);

    brush.setColor(GlobalConst::fillColor);
    painter->setBrush(brush);
    painter->drawRoundedRect(boundingRect().adjusted(1, 1, -2, -1), roundRadius, roundRadius);
}

QRectF RegexGraphicsNode::drawLabelArea(QPainter *painter, QBrush brush, QPen pen)
{
    painter->setFont(GlobalConst::labelFont);

    brush.setStyle(Qt::SolidPattern);
    brush.setColor(GlobalConst::separatorColor);
    pen.setColor(GlobalConst::separatorColor);
    painter->setPen(pen);
    painter->setBrush(brush);

    float labelHeight = painter->boundingRect(QRectF(), Qt::AlignVCenter | Qt::AlignLeft, label).height();
    QRectF labelRect = boundingRect().adjusted(0, 1, -2, -boundingRect().height() + labelHeight);
    painter->drawRoundedRect(labelRect, roundRadius, roundRadius);
    painter->drawRect(labelRect.adjusted(0, roundRadius, 0, 0));

    brush.setColor(GlobalConst::fillColor);
    painter->setBrush(brush);
    pen.setColor(GlobalConst::fillColor);
    painter->setPen(pen);

    QRectF textRect = labelRect.adjusted(8, 0, 0, 0);
    QRectF adjustedTextRect = painter->boundingRect(textRect, Qt::AlignVCenter | Qt::AlignLeft, label);
    changeBoundingRect(boundingRect() | adjustedTextRect.adjusted(0, 0, 8, 0));
    adjustedTextRect.setLeft(6);
    adjustedTextRect.setRight(bounding.width());
    painter->drawText(adjustedTextRect, Qt::AlignVCenter| Qt::AlignLeft, label);
    return labelRect;
}

QRectF RegexGraphicsNode::drawText(QPainter *painter, QBrush brush, QPen pen, QRectF textArea)
{
    brush.setColor(GlobalConst::separatorColor);
    painter->setBrush(brush);
    pen.setColor(GlobalConst::separatorColor);
    painter->setPen(pen);
    QRectF adjustedRect = painter->boundingRect(textArea, Qt::AlignCenter, text);
    changeBoundingRect(boundingRect() | adjustedRect.adjusted(0, 0, 8, 0));
    adjustedRect.setLeft(0);
    adjustedRect.setRight(bounding.width());
    painter->drawText(adjustedRect, Qt::AlignCenter, text);
    return adjustedRect;
}


int RegexGraphicsNode::type() const
{
    return Type;
}

QRectF RegexGraphicsNode::boundingRect() const
{
    return bounding;
}

QPainterPath RegexGraphicsNode::shape() const
{
    QPainterPath path;
    path.addRect(bounding);
    return path;
}

bool RegexGraphicsNode::changeBoundingRect(QRectF rect)
{
    rect.moveTo(0, 0);
    if (bounding != rect) {
        prepareGeometryChange();
        bounding = rect;
        if (isInputActive())
            getInput()->adjustPos();
        if (isOutputActive())
            getOutput()->adjustPos();
        shiftCollidingItems();
        return true;
    }
    return false;
}

QPixmap RegexGraphicsNode::getNodeImage()
{
    if (!scene())
        return QPixmap();
    QRectF rect = sceneBoundingRect() | mapToScene(childrenBoundingRect()).boundingRect();
    QPixmap result(rect.width(), rect.height());
    result.fill(Qt::transparent);
    QPainter painter(&result);
    painter.setRenderHint(QPainter::Antialiasing);
    scene()->render(&painter, QRectF(), rect);
    return result;
}



void RegexGraphicsNode::constructPopupMenu()
{
    if (popupMenu.isEmpty()) {
        popupMenu.addAction("Remove", this, SLOT(removeByUser()), QKeySequence("X,Delete"));
        if (hasProperties()) {
            popupMenu.addSeparator();
            popupMenu.addAction("Properties", this, SLOT(emitPropertiesSignal()));
        }
    }
}

void RegexGraphicsNode::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (!inToolbox) {
        constructPopupMenu();
        popupMenu.exec(event->screenPos());
    }
}

void RegexGraphicsNode::emitPropertiesSignal(RegexNodeConnection *conn)
{
    if (conn == NULL)
        emit openPropertyEditor(this);
    else
        emit openPropertyEditor(conn);
}



void RegexGraphicsNode::enableMovement(bool enabled)
{
    setFlag(QGraphicsObject::ItemIsMovable, enabled);
}


