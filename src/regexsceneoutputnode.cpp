#include "regexsceneoutputnode.h"
#include <QPainter>
#include "globalconstants.h"
#include "graphscene.h"
#include "regexgraphicscontainernode.h"

RegexSceneOutputNode::RegexSceneOutputNode(QGraphicsItem *parent) :
    RegexAbstractNode(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable, false);
    bounding = QRectF(QPointF(0, 0), QPointF(40, 200));
    activateInput();
}


QVariant RegexSceneOutputNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if (QGraphicsItem::ItemSceneChange == change) {
        if (value.value<QGraphicsScene *>() != 0) {
            QGraphicsScene *newScene = value.value<QGraphicsScene *>();
            setPos(newScene->width() - 40, newScene->height() / 2 - boundingRect().height() / 2);
        }
        return value;
    }
    return QGraphicsObject::itemChange(change, value);
}


void RegexSceneOutputNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    static const QPointF poly[5] = {
        QPointF(40, 0),
        QPointF(40, 200),
        QPointF(20, 200),
        QPointF(0, 100),
        QPointF(20, 0)
    };


    painter->setBrush(GlobalConst::fillColor);
    painter->setPen(GlobalConst::shadowColor);
    painter->drawPolygon(poly, 5);

    painter->setBrush(GlobalConst::separatorColor);
    painter->setPen(GlobalConst::separatorColor);
    painter->setFont(GlobalConst::labelFont);
    painter->drawText(boundingRect().translated(5, 0), Qt::AlignCenter, "E\nN\nD");

}

QRectF RegexSceneOutputNode::boundingRect() const
{
    return bounding;
}

QString RegexSceneOutputNode::generateRegex()
{
    return QString("");
}

int RegexSceneOutputNode::type() const
{
    return Type;
}
