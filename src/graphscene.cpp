#include "graphscene.h"
#include <QApplication>
#include <QDesktopWidget>
#include "algorithms.h"
#include <QPainter>
#include <QPixmap>
#include <QKeyEvent>
#include <QClipboard>
#include "xmlgraphserialization.h"
#include "graphundo.h"

GraphScene::GraphScene(QObject *parent) :
    QGraphicsScene(parent),
    rubberBand(NULL),
    dragMultiple(false),
    dragContainer(NULL),
    clipboard(QApplication::clipboard())
{
    setSceneRect(0, 0, /*QApplication::desktop()->width()*/1920, 1080); // This is placeholder, fixed width to prevent funky situations during demonstation.
    //connect(this, SIGNAL(changed(QList<QRectF>)), this, SLOT(recalculateSceneRect()));
    sceneInput = new RegexSceneInputNode(NULL);
    sceneOutput = new RegexSceneOutputNode(NULL);
    sceneOutput->getInput()->acceptManualConnections(false);
    addItem(sceneInput);
    addItem(sceneOutput);
}

GraphScene::~GraphScene()
{
    clearNodes();
}

void GraphScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (rubberBand) {
        if (rubberBand->setEndpoint(mouseEvent->scenePos()))
            setSelectionArea(rubberBand->shape());
    } else if (dragMultiple) {
        //QGraphicsScene::mouseMoveEvent(mouseEvent);
        QGraphicsScene::sendEvent(dragContainer, mouseEvent);
        mouseEvent->accept();
    } else {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void GraphScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsItem *item = itemAt(mouseEvent->scenePos());
    GraphUndo *undoAction = new GraphUndo(this);
    bool actionPushed = false;
    if (!item || (item && !item->isSelected()))
        emit hidePropertyEditor();
    if (!item) {
        if (!rubberBand) {
            clearSelection();
            clearFocus();
        }
        if (mouseEvent->button() == Qt::LeftButton) {
            if (rubberBand) {
                delete rubberBand;
                rubberBand = NULL;
            }
            rubberBand = new GraphRubberBand(mouseEvent->scenePos(), this);
        }
        mouseEvent->accept();
    } else if (item && item->isSelected()) {
        if (mouseEvent->button() == Qt::LeftButton) {
            if (selectedItems().size() < 2 &&
                   !(mouseEvent->modifiers() & Qt::ControlModifier)) {
                clearSelection();
                clearFocus();
                QGraphicsScene::mousePressEvent(mouseEvent);
                if (mouseEvent->isAccepted()) {
                    emit sceneChanged();
                    undoStack.push(undoAction);
                    actionPushed = true;
                }
            } else {
                dragMultiple = true;
                if (mouseEvent->modifiers() & Qt::ControlModifier) {
                    dragContainer = pasteInternal(copyInternal());
                } else {
                    dragContainer = new GraphDragContainer(this);
                    if (dragContainer->isEmpty())
                        dragContainer->acquire(selectedItems());
                }
                //dragContainer = new GraphDragContainer(this);
                //if (dragContainer->isEmpty())
                //    dragContainer->acquire(selectedItems());
                clearSelection();
                QGraphicsScene::sendEvent(dragContainer, mouseEvent);
                mouseEvent->accept();
                emit sceneChanged();
                undoStack.push(undoAction);
                actionPushed = true;
            }
        }
    } else {
        QGraphicsScene::mousePressEvent(mouseEvent);
        /*if (mouseEvent->isAccepted()) {
            undoStack.push(undoAction);
            actionPushed = true;
        }*/
    }
    if (!actionPushed) {
        delete undoAction;
    }
}

void GraphScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (rubberBand) {
        delete rubberBand;
        rubberBand = NULL;
        QGraphicsScene::mouseReleaseEvent(mouseEvent);
    } else if (dragMultiple) {
        dragMultiple = false;
        QGraphicsScene::sendEvent(dragContainer, mouseEvent);
        dragContainer = NULL;
    } else {
        QGraphicsScene::mouseReleaseEvent(mouseEvent);
    }

}

void GraphScene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    if (isAcceptedDrop(event)) {
        QString mimeText = event->mimeData()->text();
        qDebug("%s", mimeText.toAscii().data());
        int type = mimeText.section(":", 1, 1).toInt();
        qDebug("%i", type);
        QPointF scenePos = event->scenePos();
        undoStack.push(new GraphUndo(this));
        RegexGraphicsNode* newNode = RegexGraphicsNodeFactory::get(type);
        addRegexNode(newNode,scenePos);

        if (!newNode->parentItem() && getSceneInput()->getOutput()->connectionsCount() == 0)
            RegexNodeConnection::createConnection(getSceneInput(), newNode);
        event->acceptProposedAction();
    } else {
        event->ignore();
        QGraphicsScene::dropEvent(event);
    }
}

void GraphScene::addRegexNode(RegexGraphicsNode *newNode, QPointF scenePos)
{
    addItem(newNode);
    newNode->setPos(scenePos);
    newNode->changeContainer();
    newNode->shiftCollidingItems();
    clearSelection();
    newNode->setSelected(true);
    newNode->setFocus();
    connect(newNode, SIGNAL(destroyed(QObject*)), this, SLOT(removeRegexNodeFromContainer(QObject*)));
    addNodeToContainer(newNode);

}

void GraphScene::addNodeToContainer(RegexGraphicsNode *node)
{
    regexNodeContainer.append(node);
}

bool GraphScene::isAcceptedDrop(QGraphicsSceneDragDropEvent *event)
{
    return event->mimeData()->hasText()
            && event->mimeData()->text().contains(RegexGraphicsNode::NODE_FROM_TOOLBOX_MIME_DATA);
}

void GraphScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    if (isAcceptedDrop(event)) {
        event->accept();
    } else {
        event->ignore();
        QGraphicsScene::dragEnterEvent(event);
    }
}


void GraphScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->ignore();
    QGraphicsScene::dragLeaveEvent(event);
}

void GraphScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
    if (event->mimeData()->hasText()) {
        if (event->mimeData()->text().contains(RegexGraphicsNode::NODE_FROM_TOOLBOX_MIME_DATA)) {
            event->accept();
        } else {
            event->ignore();
            QGraphicsScene::dragMoveEvent(event);
        }
    } else {
        event->ignore();
        QGraphicsScene::dragMoveEvent(event);
    }
}

void GraphScene::generateRegex()
{
    bool previousNodeGrouped = true;
    bool ok;
    generatedRegex = Algorithms::generateRegex(sceneInput, sceneOutput, &previousNodeGrouped, &ok);
    emit regexChanged(generatedRegex, ok);
}


RegexSceneInputNode *GraphScene::getSceneInput() const
{
    return sceneInput;
}

void GraphScene::recalculateSceneRect()
{
    QRectF newSceneRect = itemsBoundingRect() | sceneRect();
    if (newSceneRect != sceneRect())
        setSceneRect(newSceneRect);
}

RegexSceneOutputNode *GraphScene::getSceneOutput() const
{
    return sceneOutput;
}

void GraphScene::removeRegexNodeFromContainer(QObject *node)
{
    regexNodeContainer.removeOne((RegexAbstractNode *)(node));
}

class AutoConnectionsCreator
{
public:
    AutoConnectionsCreator()
    {
    }
    bool operator()(RegexAbstractNode* node)
    {
        if (node->type() == RegexGraphicsContainerNode::Type) {
            RegexGraphicsContainerNode *container = qgraphicsitem_cast<RegexGraphicsContainerNode *>(node);
            Algorithms::breadthFirstSearch(container->getFakeInputNode(), *this);
        }
        if (node->isOutputActive() && node->getOutput()->connectionsCount() == 0) {
            disconnectedOutputs.append(node->getOutput());
        }
        return false;
    }
    QList<RegexNodeOutput* > disconnectedOutputs;
};

void GraphScene::makeAutoConnections(RegexAbstractNode *start)
{
    if (start == NULL)
        start = getSceneInput();
    AutoConnectionsCreator functor;
    Algorithms::breadthFirstSearch(start, functor);
    foreach (RegexNodeOutput* out, functor.disconnectedOutputs) {
        out->addFakeOutputConnection();
    }
}

void GraphScene::removeAutoConnections()
{
    foreach (RegexAbstractNode *node, getRegexNodes()) {
        if (node->isOutputActive()) {
            node->getOutput()->removeFakeConnections();
        }
    }
}

QList<RegexAbstractNode *> GraphScene::getRegexNodes()
{
    return regexNodeContainer;
}

void GraphScene::clearNodes()
{
    removeAutoConnections();
    QList<RegexAbstractNode *> bottom = findBottomLevel<RegexAbstractNode>(regexNodeContainer);
    foreach (RegexAbstractNode *node, bottom)
        delete node;//node->deleteLater();
    regexNodeContainer.clear();
}

QPixmap GraphScene::getImage(const QRectF &renderRect)
{
    QRectF rect;
    if (renderRect.isNull())
        rect = sceneRect();
    else
        rect = renderRect;
    QPixmap result(rect.width(), rect.height());
    result.fill(Qt::white);
    QPainter painter(&result);
    painter.setRenderHint(QPainter::Antialiasing);
    render(&painter, QRectF(), rect);
    return result;
}

void GraphScene::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    if (key == Qt::Key_Delete || key == Qt::Key_X) { // Remove
        undoStack.push(new GraphUndo(this));
        QList<QGraphicsItem *> bottomLevel = findBottomLevel<QGraphicsItem>(selectedItems());
        //foreach (QGraphicsItem * node, bottomLevel)
        //    QGraphicsScene::sendEvent(node, event);
        removeSelected();
    }

}

template <typename SomeGraphicsItem>
QList<SomeGraphicsItem *> GraphScene::findBottomLevel(QList<SomeGraphicsItem *> items)
{
    /** @badcode  Unoptimal code. It may slow down the programm if you select many object. */
    QList<SomeGraphicsItem *> bottomLevel;
    foreach (SomeGraphicsItem * node, items) {
        bool bottom = true;
        foreach (SomeGraphicsItem *maybe, items) {
            if (!bottom)
                break;
            bottom = !maybe->isAncestorOf(node);
        }
        if (bottom)
            bottomLevel.append(node);
    }
    return bottomLevel;
}

void GraphScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QGraphicsItem *clicked = itemAt(event->scenePos());
    if (selectedItems().size() > 1) {
        QMenu menu;
        QAction *action = menu.addAction("Remove");
        action->setShortcut(QKeySequence("X, Del"));
        connect(action, SIGNAL(triggered()), this, SLOT(removeSelected()));
        menu.exec(event->screenPos());
    } else if(clicked && selectedItems().isEmpty()) {
        clicked->setSelected(true);
        QGraphicsScene::sendEvent(clicked, event);
    } else {
        QGraphicsScene::contextMenuEvent(event);
    }
}

void GraphScene::removeSelected()
{
    QList<QGraphicsItem *> items = findBottomLevel(selectedItems());
    foreach (QGraphicsItem *item, items) { // First remove all connections
        if (item->type() == RegexGraphicsConnectionArc::Type)
            delete qgraphicsitem_cast<RegexGraphicsConnectionArc *>(item)->getOwner();
    }

    items = findBottomLevel(selectedItems());
    foreach (QGraphicsItem *item, items) {
        if (item->type() == RegexGraphicsContainerNode::Type || item->type() == RegexGraphicsNode::Type){
            static_cast<RegexAbstractNode *>(item)->removeNode();
        } else {
            item->toGraphicsObject()->deleteLater();
        }
    }
}

void GraphScene::selectAll()
{
    QPainterPath path;
    path.addRect(sceneRect());
    setSelectionArea(path);
}

void GraphScene::copySelection()
{
    QString serialized = copyInternal();
    QMimeData *mimeData = new QMimeData();
    mimeData->setData("text/regex",  serialized.toUtf8());
    mimeData->setImageData(getImage(selectionArea().boundingRect()));
    clipboard->setMimeData(mimeData);
    makeAutoConnections();
}

void GraphScene::paste(QPointF origin)
{
    QString text;
    const QMimeData *mimeData = clipboard->mimeData();
    text = QString::fromUtf8(mimeData->data("text/regex"));
    if (!text.isEmpty()) {
        undoStack.push(new GraphUndo(this));
        createUndoAction();
        GraphDragContainer *pasteContainer;
        pasteContainer = pasteInternal(text);
        pasteContainer->setPos(origin);
        pasteContainer->shiftCollidingItems();
        pasteContainer->release();
        delete pasteContainer;
    }
}

QString GraphScene::copyInternal()
{
    QString serialized;
    QXmlStreamWriter writer(&serialized);
    writer.setAutoFormatting(true);
    QList<RegexAbstractNode *> nodes = GraphDragContainer::getPureNodes(selectedItems());
    //removeAutoConnections();
    XMLGraphSerialization::startSerialization(writer);
    XMLGraphSerialization::serializeNodeList(nodes, writer);
    XMLGraphSerialization::endSerialization(writer);
    return serialized;
}

GraphDragContainer * GraphScene::pasteInternal(QString str)
{
    QHash<QString, RegexGraphicsNode *> nodes;
    XMLGraphSerialization::deserializeGraph(str, this, nodes);
    GraphDragContainer *pasteContainer = new GraphDragContainer(this);
    QList<QGraphicsItem *> items;
    foreach (RegexAbstractNode *node, nodes.values()) {
        items.append(node);
    }
    clearSelection();
    pasteContainer->acquire(items);
    return pasteContainer;
}

void GraphScene::clearUndoStack()
{
    undoStack.clear();
}

void GraphScene::createUndoAction()
{
    undoStack.push(new GraphUndo(this));
}

QUndoStack *GraphScene::getUndoStack()
{
    return &undoStack;
}
