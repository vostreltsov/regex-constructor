#include <QApplication>
#include "mainwindow.h"
#include "translation.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName(APP_TITLE);
    MainWindow w;
    w.show();
    w.setWindowTitle("");
    w.actionNew_triggered();
    return a.exec();
}
